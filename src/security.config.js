import passport from 'passport';
import LocalStrategy from 'passport-local';
import bcrypt from 'bcryptjs'
import GoogleStrategy from 'passport-google-oauth';
import FacebookStrategy from 'passport-facebook';
import path from 'path';

export default (app, db) => {
    passport.use(new LocalStrategy( async (username, password, done) => {
        try {
            const result = await db.user.find({username})
            if (result.length === 0){
                return done(null, false, { message: 'Incorrect username.' });
            }
            const user = result[0];
            if (!bcrypt.compareSync(password, user.password)){
                return done(null, false, { message: 'Incorrect password.' });
            }

            return done(null, user);
        }catch(err) {
            return done(err);
        };
    }));
    
    passport.serializeUser(function(user, done) {
        done(null, user.user_id);
    });
      
    passport.deserializeUser( async function(id, done) {
        try {
            const user = await db.user.find(id)
            return done(null, user)
        } catch(err){
            return done(err);
        };
    });

    const googleConfig = {
        clientID: process.env.GOOGLE_CLIENT_ID,
        clientSecret: process.env.GOOGLE_CLIENT_SECRET,
        callbackURL: process.env.GOOGLE_CALLBACK_URL
    };

    passport.use(new GoogleStrategy.OAuth2Strategy(googleConfig, createSocialLoginHandler('google', db)));
    
    const facebookConfig = {
        clientID: process.env.FACEBOOK_CLIENT_ID,
        clientSecret: process.env.FACEBOOK_CLIENT_SECRET,
        callbackURL: process.env.FACEBOOK_CALLBACK_URL,
        enableProof: true,
        profileFields: ['id', 'emails', 'name']
    };

    passport.use(new FacebookStrategy.Strategy(facebookConfig, createSocialLoginHandler('facebook', db)));

    app.use(passport.initialize());
    app.use(passport.session());

    app.get('/authentication/google/start', passport.authenticate('google', { scope: ['openid', 'profile', 'email'] }));
    app.get('/authentication/google/redirect', passport.authenticate('google'), (req, res) => { 
        req.session.save(() => { 
            res.redirect("/");
        })
    });
    
    app.get('/authentication/facebook/start', passport.authenticate('facebook', { scope: ['public_profile', 'email'] }));
    app.get('/authentication/facebook/redirect', passport.authenticate('facebook'), (req, res) => { 
        req.session.save(() => { 
            res.redirect("/"); 
        });
    });
    
    app.get('/login', function(req, res, next) {
        res.render('login');
    })

    app.post('/login', function(req, res, next) {
        passport.authenticate('local', function(err, user, info) {
            if (err) { return next(err); }
            if (!user) { 
                return res.render('login', { errorMessage: info.message });
            }
          
            req.logIn(user, function(err) {
                if (err) { return next(err); }

                req.session.save(() => { 
                    //because with pgSession there is sopme race condition
                    //between saving a session to db and new request from this redirect
                    return res.redirect('/');
                })
            });
        })(req, res, next);
    });

    app.get('/logout', function(req, res){
        req.logout();
        req.session.save(() => { 
            //because with pgSession there is sopme race condition
            //between saving a session to db and new request from this redirect
            return res.redirect('/');
        })
    });

    app.get('/signup', function(req, res){
        res.render("signup");
    })
    
    app.get('/recover', function(req, res){
        res.render("recover");
    })
}
const createSocialLoginHandler = (provider, db) => {
    return async function (accessToken, refreshToken, profile, done) {
        //profile.id, profile.emails[0].value, profile.name.familyName, profile.name.givenName
        try {
            const users = await db.userScripts.bySocialId({ provider_user_id: profile.id, provider })
            if (!users.length){
                let user = await db.user.findOne({email: profile.emails[0].value})
                if (!user){
                    const username = `${profile.name.givenName} ${profile.name.familyName}`
                    user = await db.user.insert({ email:profile.emails[0].value, family_name: profile.name.familyName, given_name: profile.name.givenName, username });
                } 
                await db.user_social_credential.insert({
                    user_id: user.user_id,
                    provider, provider_user_id: profile.id
                })
                
                done(null, user);
                
            } else {
                done(null, users[0]); 
            }
        } catch(err) {
            done(err, null);
        }
    };
}

export const createPasswordHash = (password) => {
    const salt = bcrypt.genSaltSync(10);
    return bcrypt.hashSync(password, salt);
}

export const isAuthenticatedMiddleware = (req, res, next) => {
    if (req.isAuthenticated()){
        return next();
    }

    res.redirect('/login');
}