SELECT * FROM user_match um 
    INNER JOIN match USING (match_id) 
    INNER JOIN match_player mp USING (match_id) 
    INNER JOIN user_match_player USING (player_id)
ORDER BY um.match_id;