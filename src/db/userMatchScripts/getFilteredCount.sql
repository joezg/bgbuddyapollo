SELECT count(*) 
FROM user_match_with_location_game 
WHERE user_id = ${user_id} 
	AND (
        "location" ~* ${text} 
        OR game ~* ${text}  
        OR match_id IN (SELECT DISTINCT match_id FROM user_match_player_with_buddy WHERE name ~* ${text}))
;