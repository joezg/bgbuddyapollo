SELECT user_id, u.username, u.email, u.member_since
FROM public.user u
INNER JOIN user_social_credential using (user_id)
WHERE provider_user_id = ${provider_user_id}
	AND provider = ${provider}