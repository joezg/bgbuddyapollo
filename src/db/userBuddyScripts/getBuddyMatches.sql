SELECT * 
FROM user_match_player_with_buddy 
INNER JOIN user_match_with_location_game m USING (match_id) 
WHERE m.user_id = ${user_id} AND buddy_id = ${buddyId}
ORDER BY date DESC;