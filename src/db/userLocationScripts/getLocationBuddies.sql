SELECT 
	buddy_id, name,
	count(*) match_count, 
	count(*) FILTER (WHERE winner) win_count
FROM user_match 
INNER JOIN user_match_player_with_buddy USING (match_id) 
WHERE location_id = ${locationId} AND user_match.user_id = ${user_id} AND buddy_id IS NOT NULL 
GROUP BY buddy_id, name
ORDER BY name ASC;