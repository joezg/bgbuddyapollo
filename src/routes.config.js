import migrateOldBgBuddy from './connectors/migration/oldBgBuddy.connector';
import exportBgStats from "./connectors/bgstats/export";
import path from 'path';
import express from 'express';
import {isAuthenticatedMiddleware} from './security.config';

export default (app, db) => {
    app.get('/migrate', isAuthenticatedMiddleware, function (req, res) {
        migrateOldBgBuddy(db).then(() => {
            res.send('Done!');      
        }).catch((error) => {
            res.send(`Error: ${error}`);
        });  
    });

    app.get('/bgstats', isAuthenticatedMiddleware, function (req, res) {
        exportBgStats(db).then((data) => {
            res.json(data);
        });
    })

    // Serve any static files
    app.use(express.static(path.join(__dirname, '../public'), {
        index: false
    }));
    
    // Handle React routing, return all requests to React app
    app.get('*', isAuthenticatedMiddleware, function(req, res) {
        res.sendFile(path.join(__dirname, '../public', 'index.html'));
    });
}