export default (spy, result) => {
    const newResult = result || Math.random();
    spy.mockResolvedValue(newResult);
    spy.resolvedValue = newResult;
}