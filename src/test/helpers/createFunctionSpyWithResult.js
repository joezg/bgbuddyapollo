export default (result) => {
    const newResult = result || Math.random();
    const spy = jest.fn().mockReturnValue(newResult);
    spy.result = newResult;

    return spy;
}