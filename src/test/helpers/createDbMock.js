export default (mockMethods) => {
    const mock = {}
    return mockMethods.reduce((mock, method) => {
        const getDefaultValue = () => method.resolvedValue !== undefined ? method.resolvedValue : `${method.name}_${method.method}_resolved`;

        const newMethodValue = getDefaultValue();
        const newMethod = jest.fn().mockName(`${method.name}_${method.method}`).mockResolvedValue(newMethodValue);

        newMethod.resolvedValue = newMethodValue;

        mock[method.name] = {
            ...mock[method.name],
            [method.method]: newMethod
        }
        return mock;

    }, {}) 
    
}