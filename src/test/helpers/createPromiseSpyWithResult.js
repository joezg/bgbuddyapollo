export default (result) => {
    const newResult = result || Math.random();
    const spy = jest.fn().mockResolvedValue(newResult);
    spy.resolvedValue = newResult;

    return spy;
}