import remapListObjectProperties from '../helpers/remapListObjectProperties';
import { matchMapper, participatingBuddyMapper } from '../dbMappers';

import createDbMock from '../../test/helpers/createDbMock';
import createFunctionSpyWithResult from '../../test/helpers/createFunctionSpyWithResult';
import LocationResolver from './LocationResolver';

jest.mock("../helpers/remapListObjectProperties");

const dbMock = createDbMock([
    { name: "user_match_with_location_game", method: "find" },
    { name: "userLocationScripts", method: "getLocationBuddies" },
    { name: "user_match", method: "count" },
]);

const remapListObjectPropertiesImplentation = createFunctionSpyWithResult();
remapListObjectProperties.mockReturnValue(remapListObjectPropertiesImplentation);

describe("LocationResolvers", () => {
    it('is initialized properly', () => {
        expect(LocationResolver).toHaveProperty("matches");
        expect(LocationResolver).toHaveProperty("matchCount");
        expect(LocationResolver).toHaveProperty("buddies");
        expect(LocationResolver).toHaveProperty("buddyCount");
    });

    describe("matches", () => {
        const USER_ID = 1, LOCATION_ID = 2;

        beforeEach(() => {
            jest.clearAllMocks();
        });
        
        it('fetches all matches for given location', () => {
            return LocationResolver.matches({user_id: USER_ID ,_id: LOCATION_ID}, {}, {db: dbMock, user_id: USER_ID}).then((finalValue) => {
                expect(dbMock.user_match_with_location_game.find).toBeCalledWith({ location_id: LOCATION_ID, user_id: USER_ID }, {order: "date DESC"});
                
                expect(remapListObjectProperties).toBeCalledWith(matchMapper)
                expect(remapListObjectPropertiesImplentation).toBeCalledWith(dbMock.user_match_with_location_game.find.resolvedValue)
                expect(finalValue).toBe(remapListObjectPropertiesImplentation.result)
            });
        });
    });

    describe("matchCount", () => {
        const USER_ID = 1, LOCATION_ID = 2;

        beforeEach(() => {
            jest.clearAllMocks();
        });
        
        it('fetches match count for given location', () => {
            return LocationResolver.matchCount({user_id: USER_ID ,_id: LOCATION_ID}, {}, {db: dbMock, user_id: USER_ID}).then((finalValue) => {
                expect(dbMock.user_match.count).toBeCalledWith({ location_id: LOCATION_ID, user_id: USER_ID });
                
                expect(finalValue).toBe(dbMock.user_match.count.resolvedValue)
            });
        });
    });

    describe("buddies", () => {
        const USER_ID = 1, LOCATION_ID = 2;

        beforeEach(() => {
            jest.clearAllMocks();
        });
        
        it('fetches all buddies for given location', () => {
            return LocationResolver.buddies({user_id: USER_ID ,_id: LOCATION_ID}, {}, {db: dbMock, user_id: USER_ID}).then((finalValue) => {
                expect(dbMock.userLocationScripts.getLocationBuddies).toBeCalledWith({ locationId: LOCATION_ID, user_id: USER_ID });
                
                expect(remapListObjectProperties).toBeCalledWith(participatingBuddyMapper)
                expect(remapListObjectPropertiesImplentation).toBeCalledWith(dbMock.userLocationScripts.getLocationBuddies.resolvedValue)
                expect(finalValue).toBe(remapListObjectPropertiesImplentation.result)
            });
        });
    });

    describe("buddyCount", () => {
        const USER_ID = 1, LOCATION_ID = 2;

        beforeEach(() => {
            jest.clearAllMocks();
        });
        
        it('fetches buddy count for given location', () => {
            const BUDDY_COUNT = 10;
            dbMock.userLocationScripts.getLocationBuddies.mockResolvedValueOnce(new Array(BUDDY_COUNT));

            return LocationResolver.buddyCount({user_id: USER_ID ,_id: LOCATION_ID}, {}, {db: dbMock, user_id: USER_ID}).then((finalValue) => {
                expect(dbMock.userLocationScripts.getLocationBuddies).toBeCalledWith({ locationId: LOCATION_ID, user_id: USER_ID });
                
                expect(finalValue).toBe(BUDDY_COUNT)
            });
        });
    });
});

