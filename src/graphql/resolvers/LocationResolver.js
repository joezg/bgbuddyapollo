import remapListObjectProperties from '../helpers/remapListObjectProperties';
import { matchMapper, participatingBuddyMapper } from '../dbMappers';

export default {
    matches: ({ user_id, _id }, {}, { db }) => db.user_match_with_location_game.find({ user_id, location_id: _id }, { order: "date DESC" }).then(remapListObjectProperties(matchMapper)),
    matchCount: ({ user_id, _id }, {}, { db }) => db.user_match.count({ user_id, location_id: _id }),
    buddies: ({ user_id, _id }, {}, { db }) => db.userLocationScripts.getLocationBuddies({ user_id: user_id, locationId: _id }).then(remapListObjectProperties(participatingBuddyMapper)),
    buddyCount: ({ user_id, _id }, {}, { db }) => db.userLocationScripts.getLocationBuddies({ user_id: user_id, locationId: _id }).then(buddies => buddies.length)
}