import remapListObjectProperties from '../helpers/remapListObjectProperties';
import { matchMapper } from '../dbMappers';

import createDbMock from '../../test/helpers/createDbMock';
import createFunctionSpyWithResult from '../../test/helpers/createFunctionSpyWithResult';
import BuddyResolver from './BuddyResolver';

jest.mock("../helpers/remapListObjectProperties");

const dbMock = createDbMock([
    { name: "userBuddyScripts", method: "getBuddyMatches" },
]);

const remapListObjectPropertiesImplentation = createFunctionSpyWithResult();
remapListObjectProperties.mockReturnValue(remapListObjectPropertiesImplentation);

describe("BuddyResolvers", () => {
    it('is initialized properly', () => {
        expect(BuddyResolver).toHaveProperty("matches");
        expect(BuddyResolver).toHaveProperty("matchCount");
    });
    
    const USER_ID = 1, BUDDY_ID = 2;

    describe("matches", () => {
        beforeEach(() => {
            jest.clearAllMocks();
        });
        
        it('fetches all matches for given buddy', () => {
            return BuddyResolver.matches({user_id: USER_ID ,_id: BUDDY_ID}, {}, {db: dbMock, user_id: USER_ID}).then((finalValue) => {
                expect(dbMock.userBuddyScripts.getBuddyMatches).toBeCalledWith({ buddyId: BUDDY_ID, user_id: USER_ID });
                
                expect(remapListObjectProperties).toBeCalledWith(matchMapper)
                expect(remapListObjectPropertiesImplentation).toBeCalledWith(dbMock.userBuddyScripts.getBuddyMatches.resolvedValue)
                expect(finalValue).toBe(remapListObjectPropertiesImplentation.result)
            });
        });
    });

    describe("matchCount", () => {
        beforeEach(() => {
            jest.clearAllMocks();
        });
        
        it('fetches match count for given buddy', () => {
            return BuddyResolver.matchCount({user_id: USER_ID ,_id: BUDDY_ID}, {}, {db: dbMock, user_id: USER_ID}).then((finalValue) => {
                expect(dbMock.userBuddyScripts.getBuddyMatches).toBeCalledWith({ buddyId: BUDDY_ID, user_id: USER_ID });
                
                expect(finalValue).toBe(dbMock.userBuddyScripts.getBuddyMatches.resolvedValue.length)
            });
        });
    });
});

