import remapObjectProperties from '../helpers/remapObjectProperties';
import { buddyMapper } from '../dbMappers';

import createDbMock from '../../test/helpers/createDbMock';
import createFunctionSpyWithResult from '../../test/helpers/createFunctionSpyWithResult';
import BuddyMutationsResolvers from './BuddyMutationsResolvers';

jest.mock("../helpers/remapObjectProperties");

const dbMock = createDbMock([
    { name: "user_buddy", method: "insert" },    
    { name: "user_match_player", method: "count" },    
    { name: "user_buddy", method: "destroy", resolvedValue: ["deletedItem"] },    
    { name: "user_buddy", method: "update" }   
]);

const remapObjectPropertiesImplentation = createFunctionSpyWithResult();
remapObjectProperties.mockReturnValue(remapObjectPropertiesImplentation);

describe("BuddyMutationsResolvers", () => {
    it('is initialized properly', () => {
        expect(BuddyMutationsResolvers).toHaveProperty("addBuddy");
        expect(BuddyMutationsResolvers).toHaveProperty("deleteBuddy");
        expect(BuddyMutationsResolvers).toHaveProperty("editBuddy");
    });

    const BUDDY_OBJECT = {
        _id: "_id" ,name: "buddy_name", isUser: true, like: true
    }
    const USER_ID = 1;

    describe("addBuddy", () => {
        beforeEach(() => {
            jest.clearAllMocks();
        });
        
        it('adds buddy with given data', () => {
            return BuddyMutationsResolvers.addBuddy({}, BUDDY_OBJECT, {db: dbMock, user_id: USER_ID}).then((finalValue) => {
                expect(dbMock.user_buddy.insert).toBeCalledWith({ 
                    name: BUDDY_OBJECT.name, user_id: USER_ID,
                    is_user: BUDDY_OBJECT.isUser, like: BUDDY_OBJECT.like
                });
                
                expect(remapObjectProperties).toBeCalledWith(buddyMapper)
                expect(remapObjectPropertiesImplentation).toBeCalledWith(dbMock.user_buddy.insert.resolvedValue)
                expect(finalValue).toBe(remapObjectPropertiesImplentation.result)
            });
        });
        
        it('adds buddy with default values when data is omitted', () => {
            const BUDDY_OBJECT_WITH_OMITTED = {
                name: "buddy_name"
            }

            return BuddyMutationsResolvers.addBuddy({}, BUDDY_OBJECT_WITH_OMITTED, {db: dbMock, user_id: USER_ID}).then((finalValue) => {
                expect(dbMock.user_buddy.insert).toBeCalledWith({ 
                    name: BUDDY_OBJECT.name, user_id: USER_ID,
                    is_user: false, like: false
                });
                
                expect(remapObjectProperties).toBeCalledWith(buddyMapper)
                expect(remapObjectPropertiesImplentation).toBeCalledWith(dbMock.user_buddy.insert.resolvedValue)
                expect(finalValue).toBe(remapObjectPropertiesImplentation.result)
            });
        });
    });

    describe("deleteBuddy", () => {
        const BUDDY_ID = 2;

        beforeEach(() => {
            jest.clearAllMocks();
        });
        
        it('deletes buddy with given id, when no matches with that buddy exists and buddy with given id exists', () => {
            dbMock.user_match_player.count.mockResolvedValueOnce(0);
            return BuddyMutationsResolvers.deleteBuddy({}, {_id: BUDDY_ID}, {user_id: USER_ID, db: dbMock}).then((finalValue) => {
                expect(dbMock.user_match_player.count).toBeCalledWith({ buddy_id: BUDDY_ID });
                expect(dbMock.user_buddy.destroy).toBeCalledWith({ buddy_id: BUDDY_ID, user_id: USER_ID });

                expect(remapObjectProperties).toBeCalledWith(buddyMapper);
                expect(remapObjectPropertiesImplentation).toBeCalledWith(dbMock.user_buddy.destroy.resolvedValue[0]);
                expect(finalValue).toBe(remapObjectPropertiesImplentation.result);
            });
        });

        it('throws when matches do exist for that buddy', () => {
            dbMock.user_match_player.count.mockResolvedValueOnce(1);
            return expect(BuddyMutationsResolvers.deleteBuddy({}, {_id: BUDDY_ID}, {user_id: USER_ID, db: dbMock})).rejects.toThrowError(/matches exists for the buddy with id/i)
        });

        it('throws when buddy with given id does not exist', () => {
            dbMock.user_match_player.count.mockResolvedValueOnce(0);
            dbMock.user_buddy.destroy.mockResolvedValueOnce([]);
            return expect(BuddyMutationsResolvers.deleteBuddy({}, {_id: BUDDY_ID}, {user_id: USER_ID, db: dbMock})).rejects.toThrowError(/no buddy with id/i)
        });
    });

    describe("editBuddy", () => {
        beforeEach(() => {
            jest.clearAllMocks();
        });
        
        it('updates group with new data', () => {
            return BuddyMutationsResolvers.editBuddy({}, BUDDY_OBJECT, {user_id: USER_ID, db: dbMock}).then((finalValue) => {
                expect(dbMock.user_buddy.update).toBeCalledWith({ user_id: USER_ID, buddy_id: BUDDY_OBJECT._id }, { 
                    name: BUDDY_OBJECT.name, is_user: BUDDY_OBJECT.isUser, like: BUDDY_OBJECT.like
                });
                
                expect(remapObjectProperties).toBeCalledWith(buddyMapper)
                expect(remapObjectPropertiesImplentation).toBeCalledWith(dbMock.user_buddy.update.resolvedValue[0])
                expect(finalValue).toBe(remapObjectPropertiesImplentation.result)
            });
        });
    });
});

