import remapObjectProperties from '../helpers/remapObjectProperties';
import { gameMapper } from '../dbMappers';

import createDbMock from '../../test/helpers/createDbMock';
import createFunctionSpyWithResult from '../../test/helpers/createFunctionSpyWithResult';
import GameMutationsResolvers from './GameMutationsResolvers';

jest.mock("../helpers/remapObjectProperties");

const dbMock = createDbMock([
    { name: "user_game", method: "insert" },    
    { name: "user_match", method: "count" },    
    { name: "user_game", method: "destroy", resolvedValue: ["deletedItem"] },    
    { name: "user_game", method: "update" }   
]);

const remapObjectPropertiesImplentation = createFunctionSpyWithResult();
remapObjectProperties.mockReturnValue(remapObjectPropertiesImplentation);

describe("GameMutationsResolvers", () => {
    it('is initialized properly', () => {
        expect(GameMutationsResolvers).toHaveProperty("addGame");
        expect(GameMutationsResolvers).toHaveProperty("deleteGame");
        expect(GameMutationsResolvers).toHaveProperty("editGame");
    });

    const GAME_OBJECT = {
        _id: "_id" ,name: "game_name", owned: true, played: true, wantToPlay: true, dateFirstPlayed: new Date(), dateLastPlayed: new Date(), previousPlaysNumber: 100, bggId: "bgg_id"
    }

    describe("addGame", () => {
        const USER_ID = 1;

        beforeEach(() => {
            jest.clearAllMocks();
        });
        
        it('adds game with given data', () => {
            return GameMutationsResolvers.addGame({}, GAME_OBJECT, {db: dbMock, user_id: USER_ID}).then((finalValue) => {
                expect(dbMock.user_game.insert).toBeCalledWith({ 
                    name: GAME_OBJECT.name, user_id: USER_ID,
                    bgg_id: GAME_OBJECT.bggId, date_first_played: GAME_OBJECT.dateFirstPlayed,
                    date_last_played: GAME_OBJECT.dateLastPlayed, owned: GAME_OBJECT.owned,
                    played: GAME_OBJECT.played, previous_plays_number: GAME_OBJECT.previousPlaysNumber,
                    want_to_play: GAME_OBJECT.wantToPlay
                });
                
                expect(remapObjectProperties).toBeCalledWith(gameMapper)
                expect(remapObjectPropertiesImplentation).toBeCalledWith(dbMock.user_game.insert.resolvedValue)
                expect(finalValue).toBe(remapObjectPropertiesImplentation.result)
            });
        });
        
        it('adds game with default values when data is omitted', () => {
            const GAME_OBJECT_WITH_OMITTED = {
                name: "game_name", dateFirstPlayed: new Date(), dateLastPlayed: new Date()
            }

            return GameMutationsResolvers.addGame({}, GAME_OBJECT_WITH_OMITTED, {db: dbMock, user_id: USER_ID}).then((finalValue) => {
                expect(dbMock.user_game.insert).toBeCalledWith({ 
                    name: GAME_OBJECT_WITH_OMITTED.name, user_id: USER_ID,
                    bgg_id: null, date_first_played: GAME_OBJECT_WITH_OMITTED.dateFirstPlayed,
                    date_last_played: GAME_OBJECT_WITH_OMITTED.dateLastPlayed, owned: false,
                    played: false, previous_plays_number: 0,
                    want_to_play: false
                });
                
                expect(remapObjectProperties).toBeCalledWith(gameMapper)
                expect(remapObjectPropertiesImplentation).toBeCalledWith(dbMock.user_game.insert.resolvedValue)
                expect(finalValue).toBe(remapObjectPropertiesImplentation.result)
            });
        });
    });

    describe("deleteGame", () => {
        const USER_ID = 1, GAME_ID = 2;

        beforeEach(() => {
            jest.clearAllMocks();
        });
        
        it('deletes game with given id, when no matches for that game exists and game with given id exists', () => {
            dbMock.user_match.count.mockResolvedValueOnce(0);
            return GameMutationsResolvers.deleteGame({}, {_id: GAME_ID}, {user_id: USER_ID, db: dbMock}).then((finalValue) => {
                expect(dbMock.user_match.count).toBeCalledWith({ game_id: GAME_ID, user_id: USER_ID });
                expect(dbMock.user_game.destroy).toBeCalledWith({ game_id: GAME_ID, user_id: USER_ID });

                expect(remapObjectProperties).toBeCalledWith(gameMapper);
                expect(remapObjectPropertiesImplentation).toBeCalledWith(dbMock.user_game.destroy.resolvedValue[0]);
                expect(finalValue).toBe(remapObjectPropertiesImplentation.result);
            });
        });

        it('throws when matches do exist for that location', () => {
            dbMock.user_match.count.mockResolvedValueOnce(1);
            return expect(GameMutationsResolvers.deleteGame({}, {_id: GAME_ID}, {user_id: USER_ID, db: dbMock})).rejects.toThrowError(/matches exists for the game with id/i)
        });

        it('throws when location with given id does not exist', () => {
            dbMock.user_match.count.mockResolvedValueOnce(0);
            dbMock.user_game.destroy.mockResolvedValueOnce([]);
            return expect(GameMutationsResolvers.deleteGame({}, {_id: GAME_ID}, {user_id: USER_ID, db: dbMock})).rejects.toThrowError(/no game with id/i)
        });
    });

    describe("editGame", () => {
        const USER_ID = 1;

        beforeEach(() => {
            jest.clearAllMocks();
        });
        
        it('updates group with new data', () => {
            return GameMutationsResolvers.editGame({}, GAME_OBJECT, {user_id: USER_ID, db: dbMock}).then((finalValue) => {
                expect(dbMock.user_game.update).toBeCalledWith({ user_id: USER_ID, game_id: GAME_OBJECT._id }, { 
                    name: GAME_OBJECT.name,
                    bgg_id: GAME_OBJECT.bggId, date_first_played: GAME_OBJECT.dateFirstPlayed,
                    date_last_played: GAME_OBJECT.dateLastPlayed, owned: GAME_OBJECT.owned,
                    played: GAME_OBJECT.played, previous_plays_number: GAME_OBJECT.previousPlaysNumber,
                    want_to_play: GAME_OBJECT.wantToPlay
                });
                
                expect(remapObjectProperties).toBeCalledWith(gameMapper)
                expect(remapObjectPropertiesImplentation).toBeCalledWith(dbMock.user_game.update.resolvedValue[0])
                expect(finalValue).toBe(remapObjectPropertiesImplentation.result)
            });
        });
    });
});

