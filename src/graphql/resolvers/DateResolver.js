import moment from 'moment';

export default {
    raw: (date) => date,
    ISO: (date) => moment(date).toISOString(),
    formatted: (date, { format }) => moment(date).format(format),
    relative: (date) => moment(date).fromNow(),
}