import remapObjectProperties from '../helpers/remapObjectProperties';
import { locationMapper } from '../dbMappers';

import createDbMock from '../../test/helpers/createDbMock';
import createFunctionSpyWithResult from '../../test/helpers/createFunctionSpyWithResult';
import LocationMutationsResolvers from './LocationMutationsResolvers';

jest.mock("../helpers/remapObjectProperties");

const dbMock = createDbMock([
    { name: "user_location", method: "insert" },    
    { name: "user_match", method: "count" },    
    { name: "user_location", method: "destroy", resolvedValue: ["deletedItem"] },    
    { name: "user_location", method: "update" }   
]);

const remapObjectPropertiesImplentation = createFunctionSpyWithResult();
remapObjectProperties.mockReturnValue(remapObjectPropertiesImplentation);

describe("LocationMutationsResolvers", () => {
    it('is initialized properly', () => {
        expect(LocationMutationsResolvers).toHaveProperty("addLocation");
        expect(LocationMutationsResolvers).toHaveProperty("deleteLocation");
        expect(LocationMutationsResolvers).toHaveProperty("editLocation");
    });

    describe("addLocation", () => {
        const USER_ID = 1, LOCATION_NAME = "location_name";

        beforeEach(() => {
            jest.clearAllMocks();
        });
        
        it('adds location with given name', () => {
            return LocationMutationsResolvers.addLocation({}, {name: LOCATION_NAME}, {db: dbMock, user_id: USER_ID}).then((finalValue) => {
                expect(dbMock.user_location.insert).toBeCalledWith({ name: LOCATION_NAME, user_id: USER_ID });
                
                expect(remapObjectProperties).toBeCalledWith(locationMapper)
                expect(remapObjectPropertiesImplentation).toBeCalledWith(dbMock.user_location.insert.resolvedValue)
                expect(finalValue).toBe(remapObjectPropertiesImplentation.result)
            });
        });
    });

    describe("deleteLocation", () => {
        const USER_ID = 1, LOCATION_ID = 2;

        beforeEach(() => {
            jest.clearAllMocks();
        });
        
        it('deletes location with given id, when no matches for that location exists and location with given id exists', () => {
            dbMock.user_match.count.mockResolvedValueOnce(0);
            return LocationMutationsResolvers.deleteLocation({}, {_id: LOCATION_ID}, {user_id: USER_ID, db: dbMock}).then((finalValue) => {
                expect(dbMock.user_match.count).toBeCalledWith({ location_id: LOCATION_ID, user_id: USER_ID });
                expect(dbMock.user_location.destroy).toBeCalledWith({ location_id: LOCATION_ID, user_id: USER_ID });

                expect(remapObjectProperties).toBeCalledWith(locationMapper);
                expect(remapObjectPropertiesImplentation).toBeCalledWith(dbMock.user_location.destroy.resolvedValue[0]);
                expect(finalValue).toBe(remapObjectPropertiesImplentation.result);
            });
        });

        it('throws when matches do exist for that location', () => {
            dbMock.user_match.count.mockResolvedValueOnce(1);
            return expect(LocationMutationsResolvers.deleteLocation({}, {_id: LOCATION_ID}, {user_id: USER_ID, db: dbMock})).rejects.toThrowError(/matches exists for the location with id/i)
        });

        it('throws when location with given id does not exist', () => {
            dbMock.user_match.count.mockResolvedValueOnce(0);
            dbMock.user_location.destroy.mockResolvedValueOnce([]);
            return expect(LocationMutationsResolvers.deleteLocation({}, {_id: LOCATION_ID}, {user_id: USER_ID, db: dbMock})).rejects.toThrowError(/no location with id/i)
        });
    });

    describe("editLocation", () => {
        const USER_ID = 1, LOCATION_ID = 2, LOCATION_NAME = "location_name";

        beforeEach(() => {
            jest.clearAllMocks();
        });
        
        it('updates location with new data', () => {
            return LocationMutationsResolvers.editLocation({}, {_id: LOCATION_ID, name: LOCATION_NAME}, {user_id: USER_ID, db: dbMock}).then((finalValue) => {
                expect(dbMock.user_location.update).toBeCalledWith({ user_id: USER_ID, location_id: LOCATION_ID }, { name: LOCATION_NAME });
                
                expect(remapObjectProperties).toBeCalledWith(locationMapper)
                expect(remapObjectPropertiesImplentation).toBeCalledWith(dbMock.user_location.update.resolvedValue[0])
                expect(finalValue).toBe(remapObjectPropertiesImplentation.result)
            });
        });
    });
});

