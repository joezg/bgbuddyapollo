import MatchResolver from './MatchResolver';
import remapListObjectProperties from '../helpers/remapListObjectProperties';
import { playerMapper, groupMapper } from '../dbMappers';

import createDbMock from '../../test/helpers/createDbMock';
import createFunctionSpyWithResult from '../../test/helpers/createFunctionSpyWithResult';

jest.mock("../helpers/remapListObjectProperties");

const dbMock = createDbMock([
    { name: "user_match_player_with_buddy", method: "find" },
    { name: "user_match_group_with_group", method: "find" },
]);

const remapListObjectPropertiesImplentation = createFunctionSpyWithResult();
remapListObjectProperties.mockReturnValue(remapListObjectPropertiesImplentation);

describe("MatchResolver", () => {
    it('is initialized properly', () => {
        expect(MatchResolver).toHaveProperty("players");
    });
    
    describe("players", () => {
        beforeEach(() => {
            jest.clearAllMocks();
        });
        
        it('calls db to fetch match players', () => {
            const USER_ID = 1, MATCH_ID = 2;
            return MatchResolver.players({user_id: USER_ID, _id: MATCH_ID}, {}, {db: dbMock}).then((finalValue) => {
                expect(dbMock.user_match_player_with_buddy.find).toBeCalledWith({match_id: MATCH_ID});
                expect(remapListObjectProperties).toBeCalledWith(playerMapper);
                expect(remapListObjectPropertiesImplentation).toBeCalledWith(dbMock.user_match_player_with_buddy.find.resolvedValue);
                expect(finalValue).toBe(remapListObjectPropertiesImplentation.result)
            });
        });
    });
});

