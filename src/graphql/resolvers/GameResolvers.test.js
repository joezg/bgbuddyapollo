import remapListObjectProperties from '../helpers/remapListObjectProperties';
import { matchMapper } from '../dbMappers';

import createDbMock from '../../test/helpers/createDbMock';
import createFunctionSpyWithResult from '../../test/helpers/createFunctionSpyWithResult';
import GameResolver from './GameResolver';

jest.mock("../helpers/remapListObjectProperties");

const dbMock = createDbMock([
    { name: "user_match_with_location_game", method: "find" },
    { name: "user_match", method: "count" }
]);

const remapListObjectPropertiesImplentation = createFunctionSpyWithResult();
remapListObjectProperties.mockReturnValue(remapListObjectPropertiesImplentation);

describe("GameResolvers", () => {
    it('is initialized properly', () => {
        expect(GameResolver).toHaveProperty("matches");
        expect(GameResolver).toHaveProperty("matchCount");
    });
    
    const USER_ID = 1, GAME_ID = 2;

    describe("matches", () => {
        beforeEach(() => {
            jest.clearAllMocks();
        });
        
        it('fetches all matches for given game', () => {
            return GameResolver.matches({user_id: USER_ID ,_id: GAME_ID}, {}, {db: dbMock, user_id: USER_ID}).then((finalValue) => {
                expect(dbMock.user_match_with_location_game.find).toBeCalledWith({ game_id: GAME_ID, user_id: USER_ID }, {"order": "date DESC"});
                
                expect(remapListObjectProperties).toBeCalledWith(matchMapper)
                expect(remapListObjectPropertiesImplentation).toBeCalledWith(dbMock.user_match_with_location_game.find.resolvedValue)
                expect(finalValue).toBe(remapListObjectPropertiesImplentation.result)
            });
        });
    });

    describe("matchCount", () => {
        beforeEach(() => {
            jest.clearAllMocks();
        });
        
        it('fetches match count for given game', () => {
            return GameResolver.matchCount({user_id: USER_ID ,_id: GAME_ID}, {}, {db: dbMock, user_id: USER_ID}).then((finalValue) => {
                expect(dbMock.user_match.count).toBeCalledWith({ game_id: GAME_ID, user_id: USER_ID });
                
                expect(finalValue).toBe(dbMock.user_match.count.resolvedValue)
            });
        });
    });
});

