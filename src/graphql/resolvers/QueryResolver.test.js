import QueryResolver from './QueryResolver';
import remapObjectProperties from '../helpers/remapObjectProperties';
import { userMapper } from '../dbMappers';

import createDbMock from '../../test/helpers/createDbMock';
import createFunctionSpyWithResult from '../../test/helpers/createFunctionSpyWithResult';

jest.mock("../helpers/remapObjectProperties");

const dbMock = createDbMock([
    { name: "user", method: "findOne" },
]);

const remapObjectPropertiesImplentation = createFunctionSpyWithResult();
remapObjectProperties.mockReturnValue(remapObjectPropertiesImplentation);

describe("QueryResolver", () => {
    it('is initialized properly', () => {
        expect(QueryResolver).toHaveProperty("viewer");
        expect(QueryResolver).toHaveProperty("bgg");
    });

    describe("viewer", () => {
        beforeEach(() => {
            jest.clearAllMocks();
        });
        
        it('calls db to fetch user when user id is in context', () => {
            const USER_ID = 1;
            return QueryResolver.viewer({}, {}, {user_id: USER_ID, db: dbMock}).then((finalResult) => {
                expect(dbMock.user.findOne).toBeCalledWith(USER_ID);
                expect(remapObjectProperties).toBeCalledWith(userMapper);
                expect(remapObjectPropertiesImplentation).toBeCalledWith(dbMock.user.findOne.resolvedValue);

                expect(finalResult).toBe(remapObjectPropertiesImplentation.result);
            });
    
        });
    
        it ('throws when user id not in context', () => {
            const context = {db: dbMock};
            expect(() => QueryResolver.viewer({}, {}, context)).toThrow();

            try {
                QueryResolver.viewer({}, {}, context);
            } catch (err) {
                expect(err.unauthenticated).toBeTruthy()
            }
        })
    })

    describe("bgg", () => {
        it('returns an empty object', () => {
            expect(QueryResolver.bgg()).toEqual({});
        });
    })
});

