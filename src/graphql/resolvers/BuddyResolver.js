import remapListObjectProperties from '../helpers/remapListObjectProperties';
import { matchMapper } from '../dbMappers';

export default {
    matches: ({ user_id, _id }, args, { db }) => db.userBuddyScripts.getBuddyMatches({ user_id: user_id, buddyId: _id }).then(remapListObjectProperties(matchMapper)),
    matchCount: ({ user_id, _id }, args, { db }) => db.userBuddyScripts.getBuddyMatches({ user_id: user_id, buddyId: _id }).then(buddies => buddies.length),
}