import { searchBgg } from '../../connectors/bgg.connector'; 

import addPromiseResult from '../../test/helpers/addPromiseResult';
import BggResolver from './BggResolver';

jest.mock("../../connectors/bgg.connector");
addPromiseResult(searchBgg);

describe("BggResolver", () => {
    it('is initialized properly', () => {
        expect(BggResolver).toHaveProperty("search");
    });

    const SEARCH_QUERY = "search_query";

    describe("search", () => {
        beforeEach(() => {
            jest.clearAllMocks();
        });
        
        it('searches bgg if query is larger than 2 characters', () => {
            return BggResolver.search({}, {query: SEARCH_QUERY}).then((finalValue) => {
                expect(searchBgg).toBeCalledWith(SEARCH_QUERY);
            });
        });
        
        it('returns empty array if query is 2 characters or less', () => {
            expect(BggResolver.search({}, {query: "ab"})).toEqual([]);
        });
    });

});

