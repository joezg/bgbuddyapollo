import remapListObjectProperties from '../helpers/remapListObjectProperties';
import { gameMapper, matchMapper, buddyMapper, locationMapper } from '../dbMappers';
import userCollectionResolversGenerator from '../helpers/userCollectionResolversGenerator'; //adds three common methods for user collections
import remapObjectProperties from '../helpers/remapObjectProperties';

export default {
    matches: ({ _id }, {first, after, text}, { db }) => {
        const criteria = { user_id: _id }
        const options = { order: "date DESC" };
        
        if (first) {
            options.limit = first;
        }
        if (after) {
            options.offset = after;
        }

        if (!text){
            return db.user_match_with_location_game.find(criteria, options).then(remapListObjectProperties(matchMapper));
        } else {
            return db.userMatchScripts.getFiltered({
                user_id: _id, text, limit: first, offset: after
            }).then(remapListObjectProperties(matchMapper));
        }
    },
    match: ({ _id: user_id }, { _id }, { db }) => db.user_match_with_location_game.findOne({ user_id, match_id: _id }).then(remapObjectProperties(matchMapper)),
    matchCount: ({_id}, { text }, {db}) => {

        const criteria = { user_id: _id }

        if (!text){
            return db.user_match_with_location_game.count(criteria);
        } else {
            return db.userMatchScripts.getFilteredCount({
                user_id: _id, text
            }).then(result => {
                return result[0].count;
            });
        }
    },
    ...userCollectionResolversGenerator({ 
        collectionName: "games", 
        objectName: "game", 
        tableName: "user_game", 
        table_id: "game_id", 
        mapper: gameMapper
    }),
    ...userCollectionResolversGenerator({ 
        collectionName: "buddies", 
        objectName: "buddy", 
        tableName: "user_buddy", 
        table_id: "buddy_id", 
        mapper: buddyMapper
    }),
    ...userCollectionResolversGenerator({ 
        collectionName: "locations", 
        objectName: "location", 
        tableName: "user_location", 
        table_id: "location_id", 
        mapper: locationMapper
    })
}