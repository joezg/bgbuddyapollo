import remapObjectProperties from '../helpers/remapObjectProperties';
import { userMapper } from '../dbMappers'

export default {
    viewer: (source, args, context) => {
        if (!context.user_id) {
            const err = new Error("Cannot fetch viewer data for annonymous user");
            err.unauthenticated = true;
            throw err;
        }
        return context.db.user.findOne(context.user_id).then(remapObjectProperties(userMapper));
    },
    bgg: () => ({}),
}