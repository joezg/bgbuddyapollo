import remapObjectProperties from '../helpers/remapObjectProperties';
import { matchMapper } from '../dbMappers';

export default {
    deleteMatch: (source, { _id }, { user_id, db }) => {
        let deletedMatch = null;

        return db.user_match_with_location_game.findOne({ 
            user_id, match_id: _id 
        }).then((matchToDelete) => {
            deletedMatch = matchToDelete;
            if (matchToDelete){
                return db.user_match_player.destroy({ match_id: _id });
            } else {
                throw new Error(`No match with id ${_id}!`);
            }
        }).then(()=>{
            return db.user_match.destroy({ user_id, match_id: _id });
        }).then(() => {
            return remapObjectProperties(matchMapper)(deletedMatch);
        })
    },
    addMatch: (source, newMatch, { user_id, db }) => {
        const match = {
            date: newMatch.date.raw,
            is_scored: newMatch.isScored,
            user_id,
            game_id: newMatch.gameId,
            location_id: newMatch.locationId
        }
        
        let match_id = null;
        return db.user_match.insert(match).then(match => {
            match_id = match.match_id;
            const allPlayerPromises = newMatch.players.map(player => {
                const newPlayer = {
                    match_id,
                    rank: player.rank,
                    result: player.result,
                    winner: player.winner,
                    buddy_id: player.buddyId,
                    is_anonymous: !!player.isAnonymous
                }
                
                return db.user_match_player.insert(newPlayer);
            });
            
            return Promise.all(allPlayerPromises);
        }).then(() => {
            return db.user_match_with_location_game.findOne({user_id, match_id}).then(remapObjectProperties(matchMapper));
        }); 
    },
    editMatch: (source, updatedMatch, { user_id, db }) => {
        let match = {
            match_id: updatedMatch._id,
            date: updatedMatch.date.raw,
            is_scored: updatedMatch.isScored,
            user_id,
            game_id: updatedMatch.gameId,
            location_id: updatedMatch.locationId
        }

        const matchUpdatePromise = db.user_match.update(match);

        const deletePlayersPromise = db.user_match_player.find({
            match_id: match.match_id
        }).then(existingPlayers => {
            const playersIdsToDelete = existingPlayers.filter((existingPlayer) => {
                return !updatedMatch.players.find((newPlayer) => newPlayer._id == existingPlayer.player_id);
            }).map(p => p.player_id);
    
            if (playersIdsToDelete.length > 0){
                return db.user_match_player.destroy({ player_id: playersIdsToDelete });
            }
        });
            
        const updateMatchPlayersPromises = updatedMatch.players.map( player => {
            const newPlayer = {
                match_id: match.match_id,
                rank: player.rank,
                result: player.result,
                winner: player.winner,
                buddy_id: player.buddyId,
                is_anonymous: !!player.isAnonymous
            };

            if (player._id){
                newPlayer.player_id = player._id;
            }
            return db.user_match_player.save(newPlayer);                
            
        });

        return Promise.all([
            matchUpdatePromise,
            deletePlayersPromise,
            ...updateMatchPlayersPromises,
        ]).then(() => {
            return db.user_match_with_location_game.findOne({user_id, match_id: match.match_id})
        }).then(remapObjectProperties(matchMapper));
    }
}