import remapListObjectProperties from '../helpers/remapListObjectProperties';
import { matchMapper } from '../dbMappers';

export default {
    matches: ({ user_id, _id }, args, { db }) => db.user_match_with_location_game.find({ user_id, game_id: _id }, { order: "date DESC" }).then(remapListObjectProperties(matchMapper)),
    matchCount: ({ user_id, _id }, args, { db }) => db.user_match.count({ user_id, game_id: _id }),
}