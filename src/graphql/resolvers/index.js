import Date from './DateResolver';
import Match from './MatchResolver';
import Buddy from './BuddyResolver';
import Game from './GameResolver';
import User from './UserResolver';
import Query from './QueryResolver';
import Location from './LocationResolver';
import Bgg from './BggResolver';
import MatchMutations from './MatchMutationsResolvers';
import GameMutations from './GameMutationsResolvers';
import BuddyMutations from './BuddyMutationsResolvers';
import LocationMutations from './LocationMutationsResolvers';

export default {
    Date, Match, Buddy, Game, User, Query, Location, Bgg,
    Mutation: {
        ...MatchMutations,
        ...BuddyMutations,
        ...GameMutations,
        ...LocationMutations,
    }
}