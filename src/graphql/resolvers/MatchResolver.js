import remapListObjectProperties from '../helpers/remapListObjectProperties';
import { playerMapper } from '../dbMappers';

export default {
    players: ({ user_id, _id }, args, { db }) => db.user_match_player_with_buddy.find({ match_id: _id }).then(remapListObjectProperties(playerMapper)),
}