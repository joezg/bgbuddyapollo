import UserResolver from './UserResolver';
import userCollectionResolversGenerator from '../helpers/userCollectionResolversGenerator';
import remapListObjectProperties from '../helpers/remapListObjectProperties';
import remapObjectProperties from '../helpers/remapObjectProperties';
import { matchMapper } from '../dbMappers';

import createDbMock from '../../test/helpers/createDbMock';
import createFunctionSpyWithResult from '../../test/helpers/createFunctionSpyWithResult';

jest.mock("../helpers/userCollectionResolversGenerator");
jest.mock("../helpers/remapListObjectProperties");
jest.mock("../helpers/remapObjectProperties");

const dbMock = createDbMock([
    { name: "user_match_with_location_game", method: "find" },
    { name: "userMatchScripts", method: "getFiltered" },
    { name: "user_match_with_location_game", method: "findOne" },
    { name: "userMatchScripts", method: "getFilteredCount" },
    { name: "user_match_with_location_game", method: "count" },
]);

const remapListObjectPropertiesImplentation = createFunctionSpyWithResult();
remapListObjectProperties.mockReturnValue(remapListObjectPropertiesImplentation);

const remapObjectPropertiesImplentation = createFunctionSpyWithResult();
remapObjectProperties.mockReturnValue(remapObjectPropertiesImplentation);

describe("UserResolver", () => {
    
    it('is initialized properly', () => {
        expect(UserResolver).toHaveProperty("matches");
        expect(UserResolver).toHaveProperty("match");
        expect(UserResolver).toHaveProperty("matchCount");
        expect(userCollectionResolversGenerator).toHaveBeenCalledTimes(3);
    });
    
    describe("matches", () => {
        beforeEach(() => {
            jest.clearAllMocks();
        });
        
        it('calls db to fetch data', () => {
            return UserResolver.matches({ _id: 1 }, {}, {db: dbMock}).then((finalResult) => {
                expect(dbMock.user_match_with_location_game.find).toBeCalledWith({ user_id: 1 }, { order: "date DESC" });
                expect(remapListObjectProperties).toBeCalledWith(matchMapper);
                expect(remapListObjectPropertiesImplentation).toBeCalledWith(dbMock.user_match_with_location_game.find.resolvedValue);
                
                expect(finalResult).toBe(remapListObjectPropertiesImplentation.result);
                
                expect(dbMock.userMatchScripts.getFiltered).not.toBeCalled();
            })
            
        });
        
        it('calls db for all data', () => {
            UserResolver.matches({ _id: 1 }, {}, {db: dbMock});
            
            expect(dbMock.user_match_with_location_game.find.mock.calls[0][1]).not.toHaveProperty('first');
            expect(dbMock.user_match_with_location_game.find.mock.calls[0][1]).not.toHaveProperty('after');
        });
        
        it('calls db for subset of data', () => {
            UserResolver.matches({ _id: 1 }, { first: 100 }, {db: dbMock});
            
            expect(dbMock.user_match_with_location_game.find.mock.calls[0][1]).toHaveProperty('limit', 100);
        });
        
        it('calls db and skips data', () => {
            UserResolver.matches({ _id: 1 }, { after: 100 }, {db: dbMock});
            
            expect(dbMock.user_match_with_location_game.find.mock.calls[0][1]).toHaveProperty('offset', 100);
        });
        
        it('calls db for all data with text search', () => {
            return UserResolver.matches({ _id: 1 }, { text: "anything" }, {db: dbMock}).then((finalResult) => {
                expect(dbMock.userMatchScripts.getFiltered).toBeCalledWith({
                    user_id: 1, text: "anything", limit: undefined, offset: undefined
                });
                
                expect(remapListObjectProperties).toBeCalledWith(matchMapper);
                
                expect(remapListObjectPropertiesImplentation).toBeCalledWith(dbMock.userMatchScripts.getFiltered.resolvedValue);
                
                expect(finalResult).toBe(remapListObjectPropertiesImplentation.result);

                expect(dbMock.user_match_with_location_game.find).not.toBeCalled();
                
            });
    
        });

        it('calls db subset of data with text search', () => {
            return UserResolver.matches({ _id: 1 }, { text: "anything", first: 100, after: 10 }, {db: dbMock}).then((finalResult) => {
                expect(dbMock.userMatchScripts.getFiltered).toBeCalledWith({
                    user_id: 1, text: "anything", limit: 100, offset: 10
                });
            });
    
        });
    })

    describe("Match", () => {
        beforeEach(() => {
            jest.clearAllMocks();
        });

        it("selects match from db and returns it to user", () => {
            return UserResolver.match({ _id: 1 }, { _id: 2 }, {db: dbMock}).then((finalResult) => {
                expect(dbMock.user_match_with_location_game.findOne).toHaveBeenCalledWith({user_id: 1, match_id: 2});
                expect(remapObjectProperties).toBeCalledWith(matchMapper);
                expect(remapObjectPropertiesImplentation).toBeCalledWith(dbMock.user_match_with_location_game.findOne.resolvedValue);
                expect(finalResult).toBe(remapObjectPropertiesImplentation.result);
            });
        });
    })
    
    describe("MatchCount", () => {
        beforeEach(() => {
            jest.clearAllMocks();
        });

        it("returns count based on text search query", () => {
            const COUNT = "count";
            dbMock.userMatchScripts.getFilteredCount.mockResolvedValueOnce({0: {count: COUNT}})

            return UserResolver.matchCount({ _id: 1 }, { text: "text" }, {db: dbMock}).then((finalResult) => {
                expect(dbMock.userMatchScripts.getFilteredCount).toHaveBeenCalledWith({"text": "text", "user_id": 1});
                expect(finalResult).toBe(COUNT);
            });
        });
        
        it("returns count when not filtered", () => {
            const COUNT = "count";
            dbMock.userMatchScripts.getFilteredCount.mockResolvedValueOnce({0: {count: COUNT}})

            return UserResolver.matchCount({ _id: 1 }, {}, {db: dbMock}).then((finalResult) => {
                expect(dbMock.user_match_with_location_game.count).toHaveBeenCalledWith({ "user_id": 1});
                expect(finalResult).toBe(dbMock.user_match_with_location_game.count.resolvedValue);
            });
        });
    })

});

