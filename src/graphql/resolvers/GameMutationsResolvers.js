import remapObjectProperties from '../helpers/remapObjectProperties';
import { gameMapper } from '../dbMappers';

export default {
    deleteGame: (source, { _id }, { user_id, db }) => {
      return db.user_match.count({user_id, game_id: _id}).then((count)=>{
        if (+count){
          throw new Error(`Matches exists for the game with id ${_id}. Delete matches before deleting the game.`);
        } else {
          return db.user_game.destroy({user_id, game_id: _id}).then((deletedGame)=>{
            if (deletedGame && deletedGame.length > 0)
              return remapObjectProperties(gameMapper)(deletedGame[0]);
            else 
              throw new Error(`No game with id ${_id}!`);
          });
        }
      });
    },
    addGame: (source, {name, owned = false, played = false, wantToPlay = false, dateFirstPlayed, dateLastPlayed, previousPlaysNumber, bggId = null}, { user_id, db }) => {
      const newGame = {
        name, owned, played, 
        want_to_play: wantToPlay, 
        user_id, 
        previous_plays_number: previousPlaysNumber || 0, 
        bgg_id: bggId, 
        date_first_played: dateFirstPlayed, 
        date_last_played: dateLastPlayed
      };

      return db.user_game.insert(newGame).then(remapObjectProperties(gameMapper)); 
    },
    editGame: (source, {_id, name, owned, played, wantToPlay, dateFirstPlayed, dateLastPlayed, previousPlaysNumber, bggId}, {user_id, db}) => {
      const game = {
        name, owned, played, 
        want_to_play: wantToPlay, 
        previous_plays_number: previousPlaysNumber, 
        bgg_id: bggId, 
        date_first_played: dateFirstPlayed, 
        date_last_played: dateLastPlayed
      };
      const query = {
        game_id: _id, user_id
      }

      return db.user_game.update(query, game).then(updates => remapObjectProperties(gameMapper)(updates[0])); 
    }
}