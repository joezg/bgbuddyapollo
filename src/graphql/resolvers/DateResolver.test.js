import moment from 'moment';
import DateResolver from './DateResolver';
import createFunctionSpyWithResult from '../../test/helpers/createFunctionSpyWithResult';

jest.mock("moment");
const toISOString = createFunctionSpyWithResult();
const format = createFunctionSpyWithResult();
const fromNow = createFunctionSpyWithResult();
moment.mockReturnValue({
    toISOString, format, fromNow
})

describe("DateResolver", () => {
    beforeEach(() => {
        jest.clearAllMocks();
    });
    
    it('is initialized properly', () => {
        expect(DateResolver).toHaveProperty("raw");
        expect(DateResolver).toHaveProperty("ISO");
        expect(DateResolver).toHaveProperty("formatted");
        expect(DateResolver).toHaveProperty("relative");
    });
    
    const TEST_DATE = new Date();

    it('resolves raw correctly', () => {
        expect(DateResolver.raw(TEST_DATE)).toBe(TEST_DATE);
    });
    
    it('resolves ISO correctly', () => {
        expect(DateResolver.ISO(TEST_DATE)).toBe(toISOString.result);
        expect(toISOString).toBeCalledWith();
        expect(moment).toBeCalledWith(TEST_DATE);
    });
    
    it('resolves formatted correctly', () => {
        const FORMAT = "format"
        expect(DateResolver.formatted(TEST_DATE, {format: FORMAT})).toBe(format.result);
        expect(format).toBeCalledWith(FORMAT);
        expect(moment).toBeCalledWith(TEST_DATE);
    });
    
    it('resolves relative correctly', () => {
        expect(DateResolver.relative(TEST_DATE)).toBe(fromNow.result);
        expect(fromNow).toBeCalledWith();
        expect(moment).toBeCalledWith(TEST_DATE);
    });


});

