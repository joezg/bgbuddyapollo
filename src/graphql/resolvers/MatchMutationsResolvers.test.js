import MatchMutationsResolver from './MatchMutationsResolvers';
import remapObjectProperties from '../helpers/remapObjectProperties';
import { matchMapper } from '../dbMappers';

import createDbMock from '../../test/helpers/createDbMock';
import createFunctionSpyWithResult from '../../test/helpers/createFunctionSpyWithResult';

jest.mock("../helpers/remapObjectProperties");

const dbMock = createDbMock([
    { name: "user_match_with_location_game", method: "findOne" },
    { name: "user_match_player", method: "destroy" },
    { name: "user_match", method: "destroy" },
    { name: "user_match", method: "insert" },
    { name: "user_match", method: "update" },
    { name: "user_match_player", method: "insert" },
    { name: "user_match_player", method: "find" },
    { name: "user_match_player", method: "save" }
]);

const remapObjectPropertiesImplentation = createFunctionSpyWithResult();
remapObjectProperties.mockReturnValue(remapObjectPropertiesImplentation);

describe("MatchMutationsResolver", () => {
    beforeEach(() => {
        jest.clearAllMocks();
    });

    it('is initialized properly', () => {
        expect(MatchMutationsResolver).toHaveProperty("deleteMatch");
        expect(MatchMutationsResolver).toHaveProperty("addMatch");
        expect(MatchMutationsResolver).toHaveProperty("editMatch");
    });

    describe("deleteMatch", () => {
        const USER_ID = 1, MATCH_ID = 2;

        it('throws when match_id not in database', () => {
            dbMock.user_match_with_location_game.findOne.mockResolvedValueOnce(null);

            return expect(MatchMutationsResolver.deleteMatch({}, {_id: MATCH_ID}, {db: dbMock, user_id: USER_ID})).rejects.toThrow(/no match with id/i);
        });

        it('deletes match if it is in database', () => {
            return MatchMutationsResolver.deleteMatch({}, {_id: MATCH_ID}, {db: dbMock, user_id: USER_ID}).then((finalValue) => {
                expect(dbMock.user_match_with_location_game.findOne).toBeCalledWith({ match_id: MATCH_ID, user_id: USER_ID });
                expect(dbMock.user_match_player.destroy).toBeCalledWith({ match_id: MATCH_ID });
                expect(dbMock.user_match.destroy).toBeCalledWith({ match_id: MATCH_ID, user_id: USER_ID });
                expect(remapObjectProperties).toBeCalledWith(matchMapper)
                expect(remapObjectPropertiesImplentation).toBeCalledWith(dbMock.user_match_with_location_game.findOne.resolvedValue)
                expect(finalValue).toBe(remapObjectPropertiesImplentation.result)
            });
        });
    });
    
    const USER_ID = 0;
    const MATCH_ID = "match_id";
    const PLAYER = {
        rank: "rank",
        result: "result",
        winner: "winner",
        buddyId: "buddyId",
        isAnonymous: "isAnonymous",
        _id: "playerId"
    };
    const NEW_MATCH = {
        date: {
            raw: "date.row"
        },
        isScored: "isScored",
        gameId: "gameId",
        locationId: "locationId",
    }

    describe("Add match", () => {
        it("saves match to database", () => {
            NEW_MATCH.players = [
                PLAYER, {}, {}, {}, {}
            ]
            dbMock.user_match.insert.mockResolvedValueOnce({match_id: MATCH_ID})
            return MatchMutationsResolver.addMatch({}, NEW_MATCH, {db: dbMock, user_id: USER_ID}).then((finalValue) => {
                expect(dbMock.user_match.insert).toBeCalledWith({
                    date: NEW_MATCH.date.raw,
                    game_id: NEW_MATCH.gameId,
                    is_scored: NEW_MATCH.isScored,
                    location_id: NEW_MATCH.locationId,
                    user_id: USER_ID
                });
                expect(dbMock.user_match_player.insert).toHaveBeenCalledTimes(5);
                expect(dbMock.user_match_player.insert).toBeCalledWith({
                    buddy_id: PLAYER.buddyId,
                    is_anonymous: !!PLAYER.isAnonymous,
                    match_id: MATCH_ID,
                    rank: PLAYER.rank,
                    result: PLAYER.result,
                    winner: PLAYER.winner
                });
                expect(dbMock.user_match_player.insert).toBeCalledWith({
                    buddy_id: undefined,
                    is_anonymous: false,
                    match_id: MATCH_ID,
                    rank: undefined,
                    result: undefined,
                    winner: undefined 
                });
                expect(dbMock.user_match_with_location_game.findOne).toBeCalledWith({
                    user_id: USER_ID,
                    match_id: MATCH_ID
                })
                expect(remapObjectProperties).toBeCalledWith(matchMapper)
                expect(remapObjectPropertiesImplentation).toBeCalledWith(dbMock.user_match_with_location_game.findOne.resolvedValue)
            })
        });
    });

    describe("Edit match", () => {
        NEW_MATCH._id = MATCH_ID;
        const EXISTING_PLAYERS = [
            {player_id: 1},
            {player_id: 2},
            {player_id: 3},
            {player_id: 4},
            {player_id: 5},
        ]
        it("updates match in database", () => {
            dbMock.user_match_player.find.mockResolvedValueOnce(EXISTING_PLAYERS);
            NEW_MATCH.players = [
                PLAYER, {}, {}, {}, {}
            ]
            return MatchMutationsResolver.editMatch({}, NEW_MATCH, {db: dbMock, user_id: USER_ID}).then((finalValue) => {
                expect(dbMock.user_match.update).toBeCalledWith({
                    date: NEW_MATCH.date.raw,
                    game_id: NEW_MATCH.gameId,
                    is_scored: NEW_MATCH.isScored,
                    location_id: NEW_MATCH.locationId,
                    user_id: USER_ID,
                    match_id: MATCH_ID
                });
                expect(dbMock.user_match_player.find).toBeCalledWith({match_id: MATCH_ID})
                expect(dbMock.user_match_player.destroy).toBeCalledWith({player_id: [1,2,3,4,5]})
                expect(dbMock.user_match_player.save).toHaveBeenCalledTimes(5);
                expect(dbMock.user_match_player.save).toBeCalledWith({
                    buddy_id: PLAYER.buddyId,
                    is_anonymous: !!PLAYER.isAnonymous,
                    match_id: MATCH_ID,
                    rank: PLAYER.rank,
                    result: PLAYER.result,
                    winner: PLAYER.winner,
                    match_id: MATCH_ID,
                    player_id: PLAYER._id
                });
                expect(dbMock.user_match_with_location_game.findOne).toBeCalledWith({
                    user_id: USER_ID,
                    match_id: MATCH_ID
                })
                expect(remapObjectProperties).toBeCalledWith(matchMapper)
                expect(remapObjectPropertiesImplentation).toBeCalledWith(dbMock.user_match_with_location_game.findOne.resolvedValue)
            })
        });

        it("doesn't delete any player if all players are in new list", () => {
            NEW_MATCH.players = [
                {_id: 1},
                {_id: 2},
                {_id: 3},
                {_id: 4},
                {_id: 5}
            ];
            dbMock.user_match_player.find.mockResolvedValueOnce(EXISTING_PLAYERS);

            return MatchMutationsResolver.editMatch({}, NEW_MATCH, {db: dbMock, user_id: USER_ID}).then((finalValue) =>{
                expect(dbMock.user_match_player.save).toHaveBeenCalledTimes(5);
                expect(dbMock.user_match_player.destroy).not.toHaveBeenCalled();
            });
        })
    })
});

