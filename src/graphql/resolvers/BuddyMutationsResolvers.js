import remapObjectProperties from '../helpers/remapObjectProperties';
import { buddyMapper } from '../dbMappers';

export default {
    deleteBuddy: (source, { _id }, { user_id, db }) => {
      return db.user_match_player.count({buddy_id: _id}).then((count)=>{
        if (+count){
          throw new Error(`Matches exists for the buddy with id ${_id}. Delete matches before deleting the buddy.`);
        } else {
          return db.user_buddy.destroy({user_id, buddy_id: _id}).then((deletedBuddy)=>{
            if (deletedBuddy && deletedBuddy.length > 0)
              return remapObjectProperties(buddyMapper)(deletedBuddy[0]);
            else 
              throw new Error(`No buddy with id ${_id}!`);
          });
        }
      });
    },
    addBuddy: (source, {name, isUser = false, like = false}, {user_id, db}) => {
      const newBuddy = {
        name, is_user: isUser, like, user_id
      };

      return db.user_buddy.insert(newBuddy).then(remapObjectProperties(buddyMapper)); 
    },
    editBuddy: (source, {_id, name, isUser, like}, {user_id, db}) => {
      const buddy = {
        name, is_user: isUser, like
      };
      const query = {
        buddy_id: _id, user_id
      }

      return db.user_buddy.update(query, buddy).then(updates => remapObjectProperties(buddyMapper)(updates[0])); 
    }
}