import { searchBgg } from '../../connectors/bgg.connector'; 

export default {
  search: (source, {query}) => {
    if (!query || query.length < 3) return [];
    return searchBgg(query);
  }
}