import remapObjectProperties from '../helpers/remapObjectProperties';
import { locationMapper } from '../dbMappers';

export default {
    deleteLocation: (source, { _id }, { user_id, db }) => {
      return db.user_match.count({user_id, location_id: _id}).then((count)=>{
        if (+count){
          throw new Error(`Matches exists for the location with id ${_id}. Delete matches before deleting the location.`);
        } else {
          return db.user_location.destroy({user_id, location_id: _id}).then((deletedLocation)=>{
            if (deletedLocation && deletedLocation.length > 0)
              return remapObjectProperties(locationMapper)(deletedLocation[0]);
            else 
              throw new Error(`No location with id ${_id}!`);
          });
        }
      });
    },
    addLocation: (source, { name }, { user_id, db }) => {
      const newLocation = {
        name, user_id
      };

      return db.user_location.insert(newLocation).then(remapObjectProperties(locationMapper)); 
    },
    editLocation: (source, {_id, name}, {user_id, db}) => {
      const location = {
        name
      };
      const query = {
        location_id: _id, user_id
      }

      return db.user_location.update(query, location).then(updates => remapObjectProperties(locationMapper)(updates[0])); 
    }
}