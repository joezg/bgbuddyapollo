export const userMapper = {
    user_id: "_id",
    member_since: "memberSince"
}

export const gameMapper = {
    game_id: "_id",
    want_to_play: "wantToPlay",
    date_first_played: "dateFirstPlayed",
    date_last_played: "dateLastPlayed",
    previous_plays_number: "previousPlaysNumber",
    bgg_id: "bggId"
}

export const matchMapper = {
    match_id: "_id",
    is_game_out_of_dust: "isGameOutOfDust",
    is_new_game: "isNewGame",
    is_scored: "isScored",
    game_id: "gameId",
    location_id: "locationId"
}

export const buddyMapper = {
    buddy_id: "_id",
    is_user: "isUser"
}

export const locationMapper = {
    location_id: "_id",
}

export const playerMapper = {
    player_id: "_id",
    buddy_id: "buddyId",
    is_user: "isUser",
    is_anonymous: "isAnonymous",
}

export const participatingBuddyMapper = {
    buddy_id: "_id",
    win_count: "winCount",
    match_count: "matchCount"
}