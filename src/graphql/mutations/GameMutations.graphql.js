import Game from '../types/Game.graphql';

const GameMutations =  `
    extend type Mutation {
        deleteGame(_id: ID!): Game!
        addGame(
            name: String!
            owned: Boolean
            played: Boolean
            wantToPlay: Boolean
            dateFirstPlayed: String
            dateLastPlayed: String
            previousPlaysNumber: Int
            bggId: Int
        ): Game!
        editGame(
            _id: ID!
            name: String!
            owned: Boolean
            played: Boolean
            wantToPlay: Boolean
            dateFirstPlayed: String
            dateLastPlayed: String
            previousPlaysNumber: Int
            bggId: Int
        ): Game!
    }
`;

export default [GameMutations, ...Game];