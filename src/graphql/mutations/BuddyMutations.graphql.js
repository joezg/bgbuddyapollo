import Buddy from '../types/Buddy.graphql';

const BuddyMutations =  `
    extend type Mutation {
        deleteBuddy(_id: ID!): Buddy!
        addBuddy(
            name: String!
            isUser: Boolean
            like: Boolean
        ): Buddy!
        editBuddy(
            _id: ID!
            name: String!
            isUser: Boolean!
            like: Boolean!
        ): Buddy!
    }
`;

export default [BuddyMutations, ...Buddy];