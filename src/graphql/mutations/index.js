export {default as MatchMutations} from './MatchMutations.graphql';
export {default as GameMutations} from './GameMutations.graphql';
export {default as BuddyMutations} from './BuddyMutations.graphql';
export {default as LocationMutations} from './LocationMutations.graphql';