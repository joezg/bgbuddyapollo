import Match from '../types/Match.graphql';

const MatchMutations =  `
    input DateInput {
        raw: String!
    }
    
    input PlayerInput {
        _id: ID
        buddyId: ID
        isAnonymous: Boolean
        rank: Int
        result: Int
        winner: Boolean
    }

    extend type Mutation {
        deleteMatch(_id: ID!): Match!
        addMatch(
            gameId: ID!
            locationId: ID
            players: [PlayerInput]!
            date: DateInput!
            isScored: Boolean!
        ): Match!
        editMatch(
            _id: ID!
            gameId: ID!
            locationId: ID
            players: [PlayerInput]!
            date: DateInput!
            isScored: Boolean!
        ): Match!
    }
`;

export default [MatchMutations, ...Match];