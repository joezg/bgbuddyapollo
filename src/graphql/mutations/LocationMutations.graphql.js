import Location from '../types/Location.graphql';

const LocationMutations =  `
    extend type Mutation {
        deleteLocation(_id: ID!): Location!
        addLocation(name: String!): Location!
        editLocation(
            _id: ID!
            name: String!
        ): Location!
    }
`;

export default [LocationMutations, ...Location];