export default (map) => {
    return (obj) => {
        const newObj = {};
        for (let prop in obj){
            if (obj.hasOwnProperty(prop)){
                if (map.hasOwnProperty(prop)){
                    newObj[map[prop]] = obj[prop];
                } else {
                    newObj[prop] = obj[prop];
                }
            }
        }
        return newObj;
    }
}