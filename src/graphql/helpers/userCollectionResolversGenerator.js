import remapListObjectProperties from '../helpers/remapListObjectProperties';
import remapObjectProperties from '../helpers/remapObjectProperties';

export default ({ collectionName, objectName, tableName, table_id, mapper }) => {
    return {
        [collectionName]: ({ _id }, { first, after, text }, { db }) => {
            const criteria = { user_id: _id }
            const options = { order: "name ASC" };
            
            if (first) {
                options.limit = first;
            }
            if (after) {
                options.offset = after;
            }

            if (text) {
                criteria["name ~*"] = text;
            }

            return db[tableName].find(criteria, options).then(remapListObjectProperties(mapper));
        },
        [objectName]: ({ _id: user_id }, { _id }, { db }) => db[tableName].findOne({ user_id, [table_id]: _id }).then(remapObjectProperties(mapper)),
        [objectName + "Count"]: ({_id}, { text }, {db}) => {
            const criteria = { user_id: _id };
            
            if (text) {
                criteria["name ~*"] = text;
            }

            return db[tableName].count(criteria);
        }
    }
}