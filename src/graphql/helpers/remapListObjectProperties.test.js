import remapObjectProperties from './remapObjectProperties';
import remapListObjectProperties from './remapListObjectProperties';

jest.mock('./remapObjectProperties');
const innerMapper = jest.fn();
remapObjectProperties.mockReturnValue(innerMapper);

describe("remapListObjectProperties", () => {

    it("remaps the object collection when executed with collection", () => {
        const COLLECTION_SIZE = 10;
        const oldCollection = new Array(COLLECTION_SIZE);
        const map = {};

        const result = remapListObjectProperties(map)(oldCollection);
        expect(result.length).toBe(COLLECTION_SIZE);
        expect(remapObjectProperties).toBeCalledWith(map);
        expect(innerMapper).toHaveBeenCalledTimes(10);
    });
})
