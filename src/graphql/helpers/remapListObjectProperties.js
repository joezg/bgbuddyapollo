import remapObjectProperties from './remapObjectProperties';

export default (map) => {
    const objectMapper = remapObjectProperties(map);
    return (list) => {
        const newList = [];
        for (let item of list){
            newList.push(objectMapper(item));
        }

        return newList;
    }
}