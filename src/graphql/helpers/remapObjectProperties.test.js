import remapObjectProperties from './remapObjectProperties';

describe("remapObjectProperties", () => {

    it("remaps the object when executed with object", () => {
        const map = { oldName: "newName", unexisting: "not_important" };
        const obj = { oldName: "oldName_value", notMapped: "notMapped_value" };

        obj.__proto__.inheritedProp = "not_important"; //silly thing to have 100% coverage in remapObjectProperties
        
        expect(remapObjectProperties(map)(obj)).toEqual({ newName: obj.oldName, notMapped: obj.notMapped });
    });
})
