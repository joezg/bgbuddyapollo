import remapListObjectProperties from '../helpers/remapListObjectProperties';
import remapObjectProperties from '../helpers/remapObjectProperties';
import userCollectionResolversGenerator from './userCollectionResolversGenerator';

import createFunctionSpyWithResult from '../../test/helpers/createFunctionSpyWithResult';
import createDbMock from '../../test/helpers/createDbMock';

jest.mock('../helpers/remapListObjectProperties');
jest.mock('../helpers/remapObjectProperties');

const remapListObjectPropertiesImplentation = createFunctionSpyWithResult();
remapListObjectProperties.mockReturnValue(remapListObjectPropertiesImplentation);

const remapObjectPropertiesImplentation = createFunctionSpyWithResult();
remapObjectProperties.mockReturnValue(remapObjectPropertiesImplentation);

const mapper = createFunctionSpyWithResult();

const CONFIG = { collectionName: "collectionName", objectName: "objectName", tableName: "tableName", table_id: "table_id", mapper }
const resolvers = userCollectionResolversGenerator(CONFIG);

const dbMock = createDbMock([
    { name: CONFIG.tableName, method: "find" }, 
    { name: CONFIG.tableName, method: "findOne" }, 
    { name: CONFIG.tableName, method: "count" }, 
]);

describe("userCollectionResolversGenerator", () => {
    beforeEach(() => {
        jest.clearAllMocks();
    });

    it("generates user collection resolvers for provided data", () => {
        expect(resolvers).toHaveProperty(CONFIG.collectionName);
        expect(resolvers).toHaveProperty(CONFIG.objectName);
        expect(resolvers).toHaveProperty(CONFIG.objectName + "Count");
    });

    const _ID = "_id";
    describe("collectionName", () => {
        it('returns full collection when called without argsuemnts', () => {
            return resolvers.collectionName({_id: _ID}, {}, {db: dbMock}).then((finalResult) => {
                expect(dbMock[CONFIG.tableName].find).toBeCalledWith({"user_id": _ID}, {order: "name ASC"});
                expect(remapListObjectProperties).toBeCalledWith(mapper);
                expect(remapListObjectPropertiesImplentation).toBeCalledWith(dbMock[CONFIG.tableName].find.resolvedValue)
                expect(finalResult).toBe(remapListObjectPropertiesImplentation.result);
            });
        })
        
        it('returns filtered collection if arguments provided', () => {
            const FILTERS = { first: 100, after: 10, text: "text" };
            
            return resolvers.collectionName({_id: _ID}, FILTERS, {db: dbMock}).then((finalResult) => {
                expect(dbMock[CONFIG.tableName].find).toBeCalledWith({"name ~*": "text", "user_id": "_id"}, {limit: 100, offset: 10, order: "name ASC"});
            });
        })
    })
    
    describe("objectName", () => {
        it('returns object by given id', () => {
            return resolvers.objectName({_id: _ID}, {}, {db: dbMock}).then((finalResult) => {
                expect(dbMock[CONFIG.tableName].findOne).toBeCalledWith({"user_id": _ID});
                expect(remapObjectProperties).toBeCalledWith(mapper);
                expect(remapObjectPropertiesImplentation).toBeCalledWith(dbMock[CONFIG.tableName].findOne.resolvedValue)
                expect(finalResult).toBe(remapObjectPropertiesImplentation.result);
            });
        })
    })
    
    describe("objectNameCount", () => {
        it('returns object count for user', () => {
            return resolvers.objectNameCount({_id: _ID}, {}, {db: dbMock}).then((finalResult) => {
                expect(dbMock[CONFIG.tableName].count).toBeCalledWith({"user_id": _ID});
                expect(finalResult).toBe(dbMock[CONFIG.tableName].count.resolvedValue);
            });
        })
        it('returns object count for filtered user, if text is provided', () => {
            const TEXT = "TEXT";
            return resolvers.objectNameCount({_id: _ID}, { text: TEXT}, {db: dbMock}).then((finalResult) => {
                expect(dbMock[CONFIG.tableName].count).toBeCalledWith({"user_id": _ID, "name ~*": TEXT});
                expect(finalResult).toBe(dbMock[CONFIG.tableName].count.resolvedValue);
            });
        })
    })
})
