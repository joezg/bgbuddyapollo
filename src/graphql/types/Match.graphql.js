import Date from './Date.graphql';
import Player from './Player.graphql';

const Match = `
    # represents one match played by user
    type Match {
        _id: ID!,
        game: String!
        gameId: ID!
        date: Date!
        isGameOutOfDust: Boolean
        isNewGame: Boolean
        isScored: Boolean
        location: String
        locationId: ID
        players: [Player]!
    }
`

export default [Match, Player, Date];