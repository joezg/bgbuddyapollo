import Match from './Match.graphql';
import Date from './Date.graphql';
import Game from './Game.graphql';
import Buddy from './Buddy.graphql';
import Location from './Location.graphql';

const User = `
    # represents the logged in user
    type User {
        _id: ID!
        username: String!
        email: String!
        memberSince: Date
        matches(
            first: Int!
            after: Int!
            text: String
        ): [Match]!
        games(
            first: Int!
            after: Int!
            text: String
        ): [Game]!
        buddies(
            first: Int!
            after: Int!
            text: String
        ): [Buddy]!
        locations(
            first: Int!
            after: Int!
            text: String
        ): [Location]!
        match(_id:ID!): Match
        game(_id:ID!): Game
        buddy(_id:ID!): Buddy
        location(_id:ID!): Location
        matchCount(text: String): Int!
        buddyCount(text: String): Int!
        gameCount(text: String): Int!
        locationCount(text: String): Int!
    }
`;

export default [User, Date, ...Match, ...Buddy, ...Game, ...Location ];