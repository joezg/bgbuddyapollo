export default `
  # gives date in multiple variants
  type Date {
    raw: String!
    ISO: String!
    formatted(format: String): String!
    relative: String!
  }
`