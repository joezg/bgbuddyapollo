import ParticipatingBuddy from './ParticipatingBuddy.graphql';
import Match from './Match.graphql';

const Location = `
    # represents a location of a match
    type Location {
        _id: ID!,
        name: String!
        buddies: [ParticipatingBuddy]!
        buddyCount: Int!
        matches: [Match]!
        matchCount: Int!
    }
`

export default [Location, ParticipatingBuddy, ...Match];