const ParticipatingBuddy = `
    # represents Buddy participating in location or group
    type ParticipatingBuddy {
        _id: ID!,
        name: String
        matchCount: Int!
        winCount: Int!
    }
`

export default ParticipatingBuddy;