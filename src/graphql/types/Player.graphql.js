export default `
    # represents one player in a Match
    type Player {
        _id: ID!
        rank: Int
        result: Int
        winner: Boolean
        isUser: Boolean
        buddyId: String
        name: String
        isAnonymous: Boolean
    }
`;