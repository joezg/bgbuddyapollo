import Match from './Match.graphql';

const Buddy = `
    # represents one buddy the user can play with
    type Buddy {
        _id: ID!,
        name: String!
        like: Boolean
        isUser: Boolean
        matches: [Match]!
        matchCount: Int!
    }
`

export default [Buddy, ...Match];