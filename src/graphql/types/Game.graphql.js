import Date from './Date.graphql';
import Match from './Match.graphql';

const Game = `
    # represents one game the user can play
    type Game {
        _id: ID!,
        name: String!
        owned: Boolean
        played: Boolean
        wantToPlay: Boolean
        dateFirstPlayed: Date
        dateLastPlayed: Date
        matches: [Match]!
        matchCount: Int!
        previousPlaysNumber: Int
        bggId: Int
    }
`

export default [Game, Date, ...Match];
