export default `
    # represents BGG API
    type Bgg {
        search(query: String!): [BggEntry]
    }

    type BggEntry {
        id: ID!
        name: String!
        year: Int
        image: String
        thumbnail: String
        maxPlayers: Int
        minPlayers: Int
        minAge: Int
        alternativeNames: [String]
    }
`