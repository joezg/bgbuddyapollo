import { makeExecutableSchema } from 'graphql-tools';
import { User, Bgg } from './types';
import { MatchMutations, GameMutations, BuddyMutations, LocationMutations } from './mutations';
import resolvers from './resolvers';

const SchemaDefinition = `
  # the schema allows the following query:
  type Query {
    viewer: User
    bgg: Bgg
  }

  # the schema allows the following mutations
  type Mutation {
    noop: Int
  }
`;

export default makeExecutableSchema({
  typeDefs: [
    SchemaDefinition, 
    ...User,
    Bgg,
    ...MatchMutations,
    ...GameMutations,
    ...BuddyMutations,
    ...LocationMutations,
  ],
  resolvers
});