import mailgunFactory from 'mailgun-js'

let mailgun = null;
export const initMailingSystem = () => {
    mailgun = mailgunFactory({apiKey: process.env.MAIL_API_KEY, domain: process.env.MAIL_DOMAIN})
}

export const sendEmail = (data) => {
    mailgun.messages().send(data, (error, body) => {
      if (error){
          console.error(error);
      } else {
          console.log(body);
      }
    });
}
