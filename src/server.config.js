import express from 'express';
import bodyParser from 'body-parser';
import session from 'express-session';
import pgSession from 'connect-pg-simple';
import { graphqlExpress, graphiqlExpress } from 'apollo-server-express';
import schema from './graphql/schema';
import massive from 'massive';
import configureSecurity from './security.config';
import configureRoutes from './routes.config';
import dotenv from 'dotenv';
import {initMailingSystem} from './service/mail';

if (process.env.NODE_ENV !== "production"){
    dotenv.load();
}

const PORT = process.env.PORT || 3001;

//****** db config 
let connectionInfo = {
    connectionString: process.env.DATABASE_URL,
    ssl: false,
    poolSize: 10
};

massive(connectionInfo, {
    scripts: './src/db'
}).then(db => {
    console.log({ massive: 'OK' });

    var app = express();
    app.set('views', './views')
    app.set('view engine', 'pug')

    app.use(bodyParser.urlencoded({ extended: false }))
    app.use(bodyParser.json());
    
    const PgSession = pgSession(session);
    app.use(session({ 
        store: new PgSession({
            pool: db.instance.$pool
        }),
        secret: process.env.PG_SESSION_SECRET, 
        resave: true, 
        saveUninitialized: false 
    }));

    configureSecurity(app, db);

    //***** graphql config
    app.use('/graphql', graphqlExpress((req) => { 
        return {
            schema,
            context: { db, user_id: req.user ? req.user.user_id : null },
            formatError: err => {
                if (err.originalError && err.originalError.unauthenticated){
                    err.unauthenticated = true;
                }
                return err;
            }
        }
    }));

    app.use('/graphiql', graphiqlExpress({
        endpointURL: '/graphql',
    }));
    
    configureRoutes(app, db);

    initMailingSystem();

    app.listen(PORT, () => {
        console.log({ express: 'OK' })
    });
}).catch((err)=>{
    console.log(err);
});

