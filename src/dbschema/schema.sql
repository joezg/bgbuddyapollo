--
-- PostgreSQL database dump
--

-- Dumped from database version 10.1
-- Dumped by pg_dump version 10.1

-- Started on 2019-02-20 18:00:46 CET

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 208 (class 1259 OID 16800)
-- Name: session; Type: TABLE; Schema: public; Owner: jurica
--

CREATE TABLE session (
    sid character varying NOT NULL,
    sess json NOT NULL,
    expire timestamp(6) without time zone NOT NULL
);


ALTER TABLE session OWNER TO jurica;

--
-- TOC entry 196 (class 1259 OID 16412)
-- Name: user; Type: TABLE; Schema: public; Owner: jurica
--

CREATE TABLE "user" (
    user_id integer NOT NULL,
    username text,
    email text NOT NULL,
    member_since date DEFAULT ('now'::text)::date NOT NULL,
    password text,
    family_name text,
    given_name text
);


ALTER TABLE "user" OWNER TO jurica;

--
-- TOC entry 197 (class 1259 OID 16419)
-- Name: user_buddy; Type: TABLE; Schema: public; Owner: jurica
--

CREATE TABLE user_buddy (
    buddy_id integer NOT NULL,
    user_id integer NOT NULL,
    name text NOT NULL,
    "like" boolean DEFAULT false NOT NULL,
    is_user boolean DEFAULT false NOT NULL
);


ALTER TABLE user_buddy OWNER TO jurica;

--
-- TOC entry 198 (class 1259 OID 16427)
-- Name: user_buddy_buddy_id_seq; Type: SEQUENCE; Schema: public; Owner: jurica
--

CREATE SEQUENCE user_buddy_buddy_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE user_buddy_buddy_id_seq OWNER TO jurica;

--
-- TOC entry 2524 (class 0 OID 0)
-- Dependencies: 198
-- Name: user_buddy_buddy_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jurica
--

ALTER SEQUENCE user_buddy_buddy_id_seq OWNED BY user_buddy.buddy_id;


--
-- TOC entry 199 (class 1259 OID 16435)
-- Name: user_game; Type: TABLE; Schema: public; Owner: jurica
--

CREATE TABLE user_game (
    game_id integer NOT NULL,
    user_id integer NOT NULL,
    name text NOT NULL,
    owned boolean DEFAULT false NOT NULL,
    played boolean DEFAULT false NOT NULL,
    want_to_play boolean DEFAULT false NOT NULL,
    date_first_played timestamp without time zone,
    date_last_played timestamp without time zone,
    previous_plays_number smallint DEFAULT 0 NOT NULL,
    bgg_id integer
);


ALTER TABLE user_game OWNER TO jurica;

--
-- TOC entry 200 (class 1259 OID 16445)
-- Name: user_game_game_id_seq; Type: SEQUENCE; Schema: public; Owner: jurica
--

CREATE SEQUENCE user_game_game_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE user_game_game_id_seq OWNER TO jurica;

--
-- TOC entry 2525 (class 0 OID 0)
-- Dependencies: 200
-- Name: user_game_game_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jurica
--

ALTER SEQUENCE user_game_game_id_seq OWNED BY user_game.game_id;


--
-- TOC entry 201 (class 1259 OID 16455)
-- Name: user_location; Type: TABLE; Schema: public; Owner: jurica
--

CREATE TABLE user_location (
    location_id integer NOT NULL,
    user_id integer NOT NULL,
    name text NOT NULL
);


ALTER TABLE user_location OWNER TO jurica;

--
-- TOC entry 202 (class 1259 OID 16464)
-- Name: user_location_location_id_seq; Type: SEQUENCE; Schema: public; Owner: jurica
--

CREATE SEQUENCE user_location_location_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE user_location_location_id_seq OWNER TO jurica;

--
-- TOC entry 2526 (class 0 OID 0)
-- Dependencies: 202
-- Name: user_location_location_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jurica
--

ALTER SEQUENCE user_location_location_id_seq OWNED BY user_location.location_id;


--
-- TOC entry 203 (class 1259 OID 16466)
-- Name: user_match; Type: TABLE; Schema: public; Owner: jurica
--

CREATE TABLE user_match (
    match_id bigint NOT NULL,
    user_id integer NOT NULL,
    game_id integer NOT NULL,
    location_id integer,
    is_new_game boolean DEFAULT false NOT NULL,
    is_game_out_of_dust boolean DEFAULT false NOT NULL,
    date timestamp(4) without time zone,
    is_scored boolean DEFAULT false NOT NULL
);


ALTER TABLE user_match OWNER TO jurica;

--
-- TOC entry 204 (class 1259 OID 16480)
-- Name: user_match_match_id_seq; Type: SEQUENCE; Schema: public; Owner: jurica
--

CREATE SEQUENCE user_match_match_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE user_match_match_id_seq OWNER TO jurica;

--
-- TOC entry 2527 (class 0 OID 0)
-- Dependencies: 204
-- Name: user_match_match_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jurica
--

ALTER SEQUENCE user_match_match_id_seq OWNED BY user_match.match_id;


--
-- TOC entry 205 (class 1259 OID 16482)
-- Name: user_match_player; Type: TABLE; Schema: public; Owner: jurica
--

CREATE TABLE user_match_player (
    player_id bigint NOT NULL,
    buddy_id integer,
    is_anonymous boolean DEFAULT false NOT NULL,
    match_id bigint NOT NULL,
    rank integer,
    result numeric(4,0),
    winner boolean,
    custom_score jsonb
);


ALTER TABLE user_match_player OWNER TO jurica;

--
-- TOC entry 206 (class 1259 OID 16490)
-- Name: user_match_player_player_id_seq; Type: SEQUENCE; Schema: public; Owner: jurica
--

CREATE SEQUENCE user_match_player_player_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE user_match_player_player_id_seq OWNER TO jurica;

--
-- TOC entry 2528 (class 0 OID 0)
-- Dependencies: 206
-- Name: user_match_player_player_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jurica
--

ALTER SEQUENCE user_match_player_player_id_seq OWNED BY user_match_player.player_id;


--
-- TOC entry 212 (class 1259 OID 26674)
-- Name: user_match_player_with_buddy; Type: VIEW; Schema: public; Owner: jurica
--

CREATE VIEW user_match_player_with_buddy WITH (security_barrier='false') AS
 SELECT p.player_id,
    p.match_id,
    um.user_id,
    b.name,
    p.buddy_id,
    b.is_user,
    p.is_anonymous,
    p.result,
    p.rank,
    p.winner,
    p.custom_score
   FROM ((user_match_player p
     JOIN user_match um ON ((um.match_id = p.match_id)))
     LEFT JOIN user_buddy b ON ((p.buddy_id = b.buddy_id)));


ALTER TABLE user_match_player_with_buddy OWNER TO jurica;

--
-- TOC entry 211 (class 1259 OID 19386)
-- Name: user_match_with_location_game; Type: VIEW; Schema: public; Owner: jurica
--

CREATE VIEW user_match_with_location_game WITH (security_barrier='false') AS
 SELECT um.match_id,
    um.user_id,
    um.date,
    um.is_new_game,
    um.is_game_out_of_dust,
    um.is_scored,
    um.location_id,
    l.name AS location,
    um.game_id,
    g.name AS game
   FROM ((user_match um
     LEFT JOIN user_location l USING (location_id))
     JOIN user_game g USING (game_id));


ALTER TABLE user_match_with_location_game OWNER TO jurica;

--
-- TOC entry 209 (class 1259 OID 16828)
-- Name: user_social_credential_credential_id; Type: SEQUENCE; Schema: public; Owner: jurica
--

CREATE SEQUENCE user_social_credential_credential_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE user_social_credential_credential_id OWNER TO jurica;

--
-- TOC entry 210 (class 1259 OID 18114)
-- Name: user_social_credential; Type: TABLE; Schema: public; Owner: jurica
--

CREATE TABLE user_social_credential (
    credential_id integer DEFAULT nextval('user_social_credential_credential_id'::regclass) NOT NULL,
    user_id integer NOT NULL,
    provider text NOT NULL,
    provider_user_id text NOT NULL
);


ALTER TABLE user_social_credential OWNER TO jurica;

--
-- TOC entry 207 (class 1259 OID 16501)
-- Name: user_user_id_seq; Type: SEQUENCE; Schema: public; Owner: jurica
--

CREATE SEQUENCE user_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE user_user_id_seq OWNER TO jurica;

--
-- TOC entry 2529 (class 0 OID 0)
-- Dependencies: 207
-- Name: user_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jurica
--

ALTER SEQUENCE user_user_id_seq OWNED BY "user".user_id;


--
-- TOC entry 2345 (class 2604 OID 16503)
-- Name: user user_id; Type: DEFAULT; Schema: public; Owner: jurica
--

ALTER TABLE ONLY "user" ALTER COLUMN user_id SET DEFAULT nextval('user_user_id_seq'::regclass);


--
-- TOC entry 2348 (class 2604 OID 16504)
-- Name: user_buddy buddy_id; Type: DEFAULT; Schema: public; Owner: jurica
--

ALTER TABLE ONLY user_buddy ALTER COLUMN buddy_id SET DEFAULT nextval('user_buddy_buddy_id_seq'::regclass);


--
-- TOC entry 2353 (class 2604 OID 16505)
-- Name: user_game game_id; Type: DEFAULT; Schema: public; Owner: jurica
--

ALTER TABLE ONLY user_game ALTER COLUMN game_id SET DEFAULT nextval('user_game_game_id_seq'::regclass);


--
-- TOC entry 2354 (class 2604 OID 16507)
-- Name: user_location location_id; Type: DEFAULT; Schema: public; Owner: jurica
--

ALTER TABLE ONLY user_location ALTER COLUMN location_id SET DEFAULT nextval('user_location_location_id_seq'::regclass);


--
-- TOC entry 2357 (class 2604 OID 16508)
-- Name: user_match match_id; Type: DEFAULT; Schema: public; Owner: jurica
--

ALTER TABLE ONLY user_match ALTER COLUMN match_id SET DEFAULT nextval('user_match_match_id_seq'::regclass);


--
-- TOC entry 2360 (class 2604 OID 26689)
-- Name: user_match_player player_id; Type: DEFAULT; Schema: public; Owner: jurica
--

ALTER TABLE ONLY user_match_player ALTER COLUMN player_id SET DEFAULT nextval('user_match_player_player_id_seq'::regclass);


--
-- TOC entry 2384 (class 2606 OID 16807)
-- Name: session session_pkey; Type: CONSTRAINT; Schema: public; Owner: jurica
--

ALTER TABLE ONLY session
    ADD CONSTRAINT session_pkey PRIMARY KEY (sid);


--
-- TOC entry 2367 (class 2606 OID 16515)
-- Name: user_buddy user_buddy_pkey; Type: CONSTRAINT; Schema: public; Owner: jurica
--

ALTER TABLE ONLY user_buddy
    ADD CONSTRAINT user_buddy_pkey PRIMARY KEY (buddy_id);


--
-- TOC entry 2370 (class 2606 OID 16517)
-- Name: user_buddy user_buddy_user_id_name_key; Type: CONSTRAINT; Schema: public; Owner: jurica
--

ALTER TABLE ONLY user_buddy
    ADD CONSTRAINT user_buddy_user_id_name_key UNIQUE (user_id, name);


--
-- TOC entry 2372 (class 2606 OID 16519)
-- Name: user_game user_game_pkey; Type: CONSTRAINT; Schema: public; Owner: jurica
--

ALTER TABLE ONLY user_game
    ADD CONSTRAINT user_game_pkey PRIMARY KEY (game_id);


--
-- TOC entry 2374 (class 2606 OID 16521)
-- Name: user_game user_game_user_id_name_key; Type: CONSTRAINT; Schema: public; Owner: jurica
--

ALTER TABLE ONLY user_game
    ADD CONSTRAINT user_game_user_id_name_key UNIQUE (user_id, name);


--
-- TOC entry 2376 (class 2606 OID 16529)
-- Name: user_location user_location_pkey; Type: CONSTRAINT; Schema: public; Owner: jurica
--

ALTER TABLE ONLY user_location
    ADD CONSTRAINT user_location_pkey PRIMARY KEY (location_id);


--
-- TOC entry 2378 (class 2606 OID 16531)
-- Name: user_location user_location_user_id_name_key; Type: CONSTRAINT; Schema: public; Owner: jurica
--

ALTER TABLE ONLY user_location
    ADD CONSTRAINT user_location_user_id_name_key UNIQUE (user_id, name);


--
-- TOC entry 2380 (class 2606 OID 26661)
-- Name: user_match user_match_pk; Type: CONSTRAINT; Schema: public; Owner: jurica
--

ALTER TABLE ONLY user_match
    ADD CONSTRAINT user_match_pk PRIMARY KEY (match_id);


--
-- TOC entry 2382 (class 2606 OID 26688)
-- Name: user_match_player user_match_player_pkey; Type: CONSTRAINT; Schema: public; Owner: jurica
--

ALTER TABLE ONLY user_match_player
    ADD CONSTRAINT user_match_player_pkey PRIMARY KEY (player_id);


--
-- TOC entry 2363 (class 2606 OID 16541)
-- Name: user user_pkey; Type: CONSTRAINT; Schema: public; Owner: jurica
--

ALTER TABLE ONLY "user"
    ADD CONSTRAINT user_pkey PRIMARY KEY (user_id);


--
-- TOC entry 2386 (class 2606 OID 18121)
-- Name: user_social_credential user_social_credential_pkey; Type: CONSTRAINT; Schema: public; Owner: jurica
--

ALTER TABLE ONLY user_social_credential
    ADD CONSTRAINT user_social_credential_pkey PRIMARY KEY (credential_id);


--
-- TOC entry 2365 (class 2606 OID 16543)
-- Name: user user_username_key; Type: CONSTRAINT; Schema: public; Owner: jurica
--

ALTER TABLE ONLY "user"
    ADD CONSTRAINT user_username_key UNIQUE (username);


--
-- TOC entry 2368 (class 1259 OID 16544)
-- Name: user_buddy_user_id_is_user_idx; Type: INDEX; Schema: public; Owner: jurica
--

CREATE UNIQUE INDEX user_buddy_user_id_is_user_idx ON user_buddy USING btree (is_user, user_id) WHERE (is_user = true);


--
-- TOC entry 2387 (class 2606 OID 16575)
-- Name: user_buddy user_buddy_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: jurica
--

ALTER TABLE ONLY user_buddy
    ADD CONSTRAINT user_buddy_user_id_fkey FOREIGN KEY (user_id) REFERENCES "user"(user_id);


--
-- TOC entry 2388 (class 2606 OID 16580)
-- Name: user_game user_game_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: jurica
--

ALTER TABLE ONLY user_game
    ADD CONSTRAINT user_game_user_id_fkey FOREIGN KEY (user_id) REFERENCES "user"(user_id);


--
-- TOC entry 2389 (class 2606 OID 16605)
-- Name: user_location user_location_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: jurica
--

ALTER TABLE ONLY user_location
    ADD CONSTRAINT user_location_user_id_fkey FOREIGN KEY (user_id) REFERENCES "user"(user_id);


--
-- TOC entry 2390 (class 2606 OID 16610)
-- Name: user_match user_match_game_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: jurica
--

ALTER TABLE ONLY user_match
    ADD CONSTRAINT user_match_game_id_fkey FOREIGN KEY (game_id) REFERENCES user_game(game_id);


--
-- TOC entry 2391 (class 2606 OID 16630)
-- Name: user_match user_match_location_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: jurica
--

ALTER TABLE ONLY user_match
    ADD CONSTRAINT user_match_location_id_fkey FOREIGN KEY (location_id) REFERENCES user_location(location_id);


--
-- TOC entry 2393 (class 2606 OID 16635)
-- Name: user_match_player user_match_player_buddy_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: jurica
--

ALTER TABLE ONLY user_match_player
    ADD CONSTRAINT user_match_player_buddy_id_fkey FOREIGN KEY (buddy_id) REFERENCES user_buddy(buddy_id);


--
-- TOC entry 2394 (class 2606 OID 26662)
-- Name: user_match_player user_match_player_match_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: jurica
--

ALTER TABLE ONLY user_match_player
    ADD CONSTRAINT user_match_player_match_id_fkey FOREIGN KEY (match_id) REFERENCES user_match(match_id);


--
-- TOC entry 2392 (class 2606 OID 16650)
-- Name: user_match user_match_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: jurica
--

ALTER TABLE ONLY user_match
    ADD CONSTRAINT user_match_user_id_fkey FOREIGN KEY (user_id) REFERENCES "user"(user_id);


--
-- TOC entry 2395 (class 2606 OID 18122)
-- Name: user_social_credential user_social_credential_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: jurica
--

ALTER TABLE ONLY user_social_credential
    ADD CONSTRAINT user_social_credential_user_id_fkey FOREIGN KEY (user_id) REFERENCES "user"(user_id);


-- Completed on 2019-02-20 18:00:46 CET

--
-- PostgreSQL database dump complete
--

