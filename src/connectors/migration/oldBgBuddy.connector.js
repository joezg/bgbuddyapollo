import * as api from './oldBgBuddy.api';
import * as fetch from './oldBgBuddy.db';
import * as config from './oldBgBuddy.config';
import { createPasswordHash } from '../../security.config';

export default (db) => {
    /************** MIGRATORS ********************/
    const migrateMatches = () => {
        return api.getMatches().then((matches) => {
            const allMatchPromises = [];
            for (let match of matches){
                allMatchPromises.push(
                    Promise.all([
                        fetch.getLocationByName(db, match.location),
                        fetch.getGameByName(db, match.game)
                    ]).then(([locationId, gameId]) => {
                        const newUserMatch = {
                            user_id: 1,
                            game_id: gameId,
                            location_id: locationId,
                            is_new_game: !!match.isNewGame,
                            is_game_out_of_dust: !!match.isGameOutOfDust,
                            date: match.date,
                            is_scored: !!match.isScored
                        }

                        return db.user_match.insert(newUserMatch);
                    }).then(({match_id})=>{
                        const playerPromises = [];
                        
                        for (let player of match.players){
                            let { result, rank, winner } = player;

                            let newMatchPlayer = {
                                match_id, result, rank, winner, 
                            }

                            if (player.isAnonymous){
                                newMatchPlayer = {
                                    ...newMatchPlayer,
                                    buddy_id: null,
                                    is_anonymous: true
                                }

                                playerPromises.push(db.user_match_player.insert(newMatchPlayer));
                            } else {
                                let name = player.name;
                                if (player.isUser){
                                    name = "Jurica";   
                                }
                                
                                playerPromises.push(fetch.getBuddyByName(db, name).then((buddyId) => {
                                    newMatchPlayer = {
                                        ...newMatchPlayer,
                                        buddy_id: buddyId
                                    }

                                    return db.user_match_player.insert(newMatchPlayer);
                                }));
                            }                                           
                        }

                        return Promise.all(playerPromises);
                    })
                );
            }
            return Promise.all(allMatchPromises);
        });
    }

    const migrateGames = () => {
        return api.getGames().then((games) => {
            const allGamePromises = []
            for (let game of games){
                if (!config.skipGame(game.name)){
                    let newGame = {
                        user_id: 1,
                        name: config.mapDuplicateGameNames(game.name),
                        owned: !!game.owned,
                        played: !!game.played
                    }
                    allGamePromises.push(db.user_game.save(newGame));
                }
            }
            return Promise.all(allGamePromises);
        });
    }

    const migrateBuddies = () => {
        return api.getBuddies().then((buddies) => {
            const allBuddyPromises = [];
            allBuddyPromises.push(db.user_buddy.save({
                user_id: 1,
                name: 'Jurica',
                is_user: true
            }));
            
            for (let buddy of buddies){
                let newBuddy = {
                    user_id: 1,
                    name: buddy.name,
                    like: !!buddy.like
                }
                allBuddyPromises.push(db.user_buddy.save(newBuddy));
            }

            return Promise.all(allBuddyPromises);
        });
    } 

    fetch.resetDbPromises();
    
    const pwdHash = createPasswordHash("-salDO23");

    return db.run(`
        truncate user_match_player restart identity cascade;
        truncate user_match restart identity cascade;
        truncate user_game restart identity cascade;
        truncate user_buddy restart identity cascade;
        truncate user_location restart identity cascade;
        truncate public.user restart identity cascade;
        insert into public.user (username, email, password) values ('jurica', 'jurica.hladek@gmail.com', '${pwdHash}');
    `).then(() => {
        return migrateGames()
    }).then(()=>{
        return migrateBuddies();
    }).then(()=>{
        return migrateMatches();
    });

    
}