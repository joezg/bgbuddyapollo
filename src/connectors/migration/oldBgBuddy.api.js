import rp from 'request-promise';

export const getUrl = (action, params) => {
    const getParams = []
    for (let param in params){
        getParams.push(`${param}=${params[param]}`)
    }

    return `http://bgbuddy-old.herokuapp.com/api/${action}?${getParams.join('&')}`;
}

export const loginThenAction = () => {
    const options = {
        method: 'POST',
        uri: 'http://bgbuddy-old.herokuapp.com/api/logIn',
        body: {
            username: 'jurica',
            password: '-salDO23'
        },
        json: true
    };

    return rp(options).then((result)=>{
        return result.sessionId;
    });
}

export const getMatches = () => {
    return loginThenAction().then((sessionId)=>{
        const url = getUrl('matchGetLast', { sessionId, count: 1000000 });

        return rp({ url }).then((result) => {
            return JSON.parse(result).matches;
        })    
    });
}

export const getGames = () => {
    return loginThenAction().then((sessionId)=>{
        const url = getUrl('gamesGet', { sessionId });

        return rp({ url }).then((result) => {
            return JSON.parse(result).games;        
        })
    })
}

export const getBuddies = () => {
    return loginThenAction().then((sessionId)=>{
        const url = getUrl('buddyGetAll', { sessionId });

        return rp({ url }).then((result) => {
            return JSON.parse(result).buddies;    
        })
    })
}