const locationMap = {
    "Brk's place": "Brk",
    "Brk's place ": "Brk",
    "Casa de Brchez": "Brk",
    "Dugave": "Brk",
    "Geeknight ": "Geeknight",
    "Kod Nevena": "Neven",
    "Kod Šadija": "Šadi",
    "Mikulići": "Talani",
    "Rudeška": "Kod Eve i Siniše",
    "Štosova": "Štoosova",
    "Tisno ": "Tisno",
    "Talani ": "Talani",
    "Pbz": "PBZ"
}

const gameNamesMap = {
    "7Wonders": "7 Wonders",
    "One  Night Ultimate Vampire": "One Night Ultimate Vampire"
}

const gamesToSkip = {
    "7Wonders": true
}

export const mapDuplicateGameNames = (name) => {
    return gameNamesMap[name] || name;
}

export const skipGame = (name) => {
    return !!gamesToSkip[name];
}

export const mapDuplicateLocationNames = (name) => {
    return locationMap[name] || name;
}