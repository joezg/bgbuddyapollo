import * as config from './oldBgBuddy.config';

let getLocationsPromises = {};
let getGamesPromises = {};
let getBuddyPromises = {};

/********* FETCHERS **********/
export const resetDbPromises = () => {
    getBuddyPromises = {};
    getLocationsPromises = {};
    getGamesPromises = {};
}

export const getLocationByName = (db, name) => {
    name = config.mapDuplicateLocationNames(name);
    if (!getLocationsPromises.hasOwnProperty(name)){
        getLocationsPromises[name] = db.user_location.findOne({ name }).then((result) => {
            if (!result){
                return db.user_location.save({
                    user_id: 1, name
                }).then((result) => {
                    return result.location_id;
                });
            }

            return result.location_id;
        })
    }

    return getLocationsPromises[name];
}

export const getGameByName = (db, name) => {
    name = config.mapDuplicateGameNames(name);
    if (!getGamesPromises.hasOwnProperty(name)){
        getGamesPromises[name] = db.user_game.findOne({ name }).then((result) => {
            if (!result){
                return db.user_game.save({
                    user_id: 1, name
                }).then((result) => {
                    return result.game_id;
                });
            }

            return result.game_id;
        })
    }

    return getGamesPromises[name];
}

export const getBuddyByName = (db, name) => {
    if (!getBuddyPromises.hasOwnProperty(name)){
        getBuddyPromises[name] = db.user_buddy.findOne({ name }).then((result) => {
            if (!result){
                return db.user_buddy.save({
                    user_id: 1, name
                }).then((result) => {
                    return result.buddy_id;
                });
            }

            return result.buddy_id;
        })
    }

    return getBuddyPromises[name];
}