import { getSearchAction, getThingAction } from './bgg.helpers';

import { getBggThings, searchBgg } from './bgg.connector';
import generateRandomString from '../test/helpers/generateRandomString';

jest.mock("./bgg.helpers");

describe("bgg.connector", () => {
    beforeEach(() => {
        jest.clearAllMocks();
    });

    describe("getBggThings", ()=> {
        it("calls BGG api, sorts and map returned items", () => {
            const RESULT = "result";
            const mockedResolvedValue = {
                map: jest.fn().mockReturnValue(RESULT),
                sort: jest.fn()
            }; 
            getThingAction.mockResolvedValueOnce(mockedResolvedValue);

            return getBggThings(1).then((result) => {
                expect(result).toBe(RESULT);
                expect(getThingAction).toBeCalledWith(1);
                expect(mockedResolvedValue.sort).toBeCalledWith(expect.any(Function));
                expect(mockedResolvedValue.map).toBeCalledWith(expect.any(Function));
            });
            
        });

        describe("when receive an array", () => {
            const mockedResolvedValue = [
                { 
                    statistics: { ratings: { owned: { value: Math.random() } } },
                    name: { value: generateRandomString(), type: "primary" },
                    image: generateRandomString(), thumbnail: generateRandomString(),
                    maxplayers: { value: Math.random()}, minplayers: { value: Math.random()},
                    minage: { value: Math.random()}, yearpublished: { value: Math.random()},
                    id: Math.random()
                },
                { 
                    statistics: { ratings: { owned: { value: Math.random() } } },
                    name: { value: generateRandomString(), type: "primary" },
                    image: generateRandomString(), thumbnail: generateRandomString(),
                    id: Math.random()
                },
                { 
                    statistics: { ratings: { owned: { value: Math.random() } } },
                    name: [
                        { value: generateRandomString(), type: "secundary" }, 
                        { value: generateRandomString(), type: "secundary" }, 
                        { value: generateRandomString(), type: "primary" }
                    ],
                    image: generateRandomString(), thumbnail: generateRandomString(),
                    maxplayers: { value: Math.random()}, minplayers: { value: Math.random()},
                    minage: { value: Math.random()}, yearpublished: { value: Math.random()} ,
                    id: Math.random()
                },
                { 
                    statistics: { ratings: { owned: { value: Math.random() } } },
                    name: { value: generateRandomString(), type: "primary" },
                    image: generateRandomString(), thumbnail: generateRandomString(),
                    maxplayers: { value: Math.random()}, minplayers: { value: Math.random()},
                    minage: { value: Math.random()}, yearpublished: { value: Math.random()} ,
                    id: Math.random()
                },
                { 
                    statistics: { ratings: { owned: { value: Math.random() } } },
                    name: { value: generateRandomString(), type: "primary" },
                    image: generateRandomString(), thumbnail: generateRandomString(),
                    maxplayers: { value: Math.random()}, minplayers: { value: Math.random()},
                    minage: { value: Math.random()}, yearpublished: { value: Math.random()} ,
                    id: Math.random()
                },
            ]; 
            getThingAction.mockResolvedValue(mockedResolvedValue);

            it("sorts received array", () => {
                return getBggThings(1).then((result) => {
                    expect(Array.isArray(result)).toBe(true);
                    expect(result.length).toBe(5);

                    mockedResolvedValue.sort((a, b) => b.statistics.ratings.owned.value - a.statistics.ratings.owned.value);
                    result.map((item, i) => {
                        expect(item.id).toBe(mockedResolvedValue[i].id);
                    });

                    expect(getThingAction).toBeCalledWith(1);
                });               
            })

            it("maps received array", () => {
                return getBggThings(1).then((result) => {
                    expect(Array.isArray(result)).toBe(true);
                    expect(result.length).toBe(5);

                    mockedResolvedValue.sort((a, b) => b.statistics.ratings.owned.value - a.statistics.ratings.owned.value);
                    result.map((item, i) => {
                        expect(item.id).toBe(mockedResolvedValue[i].id);
                        expect(item.image).toBe(mockedResolvedValue[i].image);
                        expect(item.thumbnail).toBe(mockedResolvedValue[i].thumbnail);
                        expect(item.maxPlayers).toBe(mockedResolvedValue[i].maxplayers ? mockedResolvedValue[i].maxplayers.value : null);
                        expect(item.minPlayers).toBe(mockedResolvedValue[i].minplayers ? mockedResolvedValue[i].minplayers.value : null);
                        expect(item.minAge).toBe(mockedResolvedValue[i].minage ? mockedResolvedValue[i].minage.value : null);
                        expect(item.name).toBe(mockedResolvedValue[i].name.filter((name)=>name.type==="primary")[0].value);
                        expect(item.alternativeNames).toEqual(mockedResolvedValue[i].name.filter((name)=>name.type!=="primary").map((name)=>name.value));
                        expect(item.year).toBe(mockedResolvedValue[i].yearpublished ? mockedResolvedValue[i].yearpublished.value : null);
                    });

                    expect(getThingAction).toBeCalledWith(1);
                });               
            })

            it("when receives only one value, wraps it in array and proceeds like above", () => {
                getThingAction.mockResolvedValueOnce(mockedResolvedValue[0]);
                return getBggThings(1).then((result) => {
                    expect(Array.isArray(result)).toBe(true);
                    expect(result.length).toBe(1);
                    expect(getThingAction).toBeCalledWith(1);
                });               
            })
        });

    });

    describe("searchBgg", () => {

        getSearchAction.mockResolvedValue([
            {id: 1},
            {id: 2},
            {id: 2},
            {id: 3}
        ])

        const QUERY = generateRandomString();

        it("sends search request to bgg api and than fetches items for search result", () => {
            return searchBgg(QUERY).then(() => {
                expect(getSearchAction).toBeCalledWith(QUERY)
                expect(getThingAction).toBeCalledWith(expect.arrayContaining([1, 2, 3]));

                const arrayOfUniqueIds = getThingAction.mock.calls[0][0]
                expect(arrayOfUniqueIds.length).toBe(3);
            })
        })
        
        it("wraps scalar result of getSearchAction in array", () => {
            getSearchAction.mockResolvedValueOnce( {id: 1} )

            return searchBgg(QUERY).then(() => {
                expect(getSearchAction).toBeCalledWith(QUERY)
                expect(getThingAction).toBeCalledWith(expect.arrayContaining([1]));

                const arrayOfUniqueIds = getThingAction.mock.calls[0][0]
                expect(arrayOfUniqueIds.length).toBe(1);
            })
        })
    })

});
