import { getSearchAction, getThingAction } from './bgg.helpers';

export const getBggThings = (ids) => {
    return getThingAction(ids).then((items)=>{
      if (!items.map){
        items = [items];
      }

      if (items[Symbol.iterator]){
        items = [...items];
      }

      items.sort((a, b) => b.statistics.ratings.owned.value - a.statistics.ratings.owned.value);

      return items.map((item)=>{
        if (!item.name.filter) {// not an array
          item.name = [item.name];
        }
        return {
          id: item.id,
          image: item.image,
          thumbnail: item.thumbnail,
          maxPlayers: item.maxplayers ? item.maxplayers.value : null,
          minPlayers: item.minplayers ? item.minplayers.value : null,
          minAge: item.minage ? item.minage.value : null,
          name: item.name.filter((name)=>name.type==="primary")[0].value,
          alternativeNames: item.name.filter((name)=>name.type!=="primary").map((name)=>name.value),
          year: item.yearpublished ? item.yearpublished.value : null
        };
      });
    })
}

export const searchBgg = (query) => {
    return getSearchAction(query).then((items) => {
      if (!items.map){
        items = [items];
      }

      const ids = items.map( item => item.id);
      return getBggThings([ ...new Set(ids) ]);
    })
}