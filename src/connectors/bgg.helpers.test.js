import rp from 'request-promise';
import { Parser } from 'xml2js';

import addPromiseResult from '../test/helpers/addPromiseResult';
import { parseBggXml, getSearchAction, getThingAction } from './bgg.helpers';

jest.mock("xml2js");
jest.mock("request-promise");
addPromiseResult(rp);

describe("bgg.helpers", () => {
    beforeEach(() => {
        jest.clearAllMocks();
    });

    describe("parseBggXml", ()=> {
        const XML = "testXML";
        const RESULT = { item: "ParsedSuccessfully" };
        it("returnes promise which will be resolved with parsed XML", () => {
            const parsedPromise = parseBggXml(XML);
            expect(Parser).toBeCalledWith({
                explicitArray: false,
                explicitRoot: false,
                mergeAttrs: true
            });
            expect(Parser.mock.instances.length).toBe(1);
            expect(Parser.mock.instances[0].parseString).toHaveBeenCalledTimes(1);
            expect(Parser.mock.instances[0].parseString).toBeCalledWith(XML, expect.any(Function));

            expect.assertions(5);
            Parser.mock.instances[0].parseString.mock.calls[0][1](null, RESULT);
            return parsedPromise.then((result) => {
                expect(result).toBe(RESULT.item);
            }).catch(()=>{ fail(); })
        })

        it("returnes promise which will fail if error in parsing occures", () => {
            const parsedPromise = parseBggXml(XML);
            
            const error = new Error();
            
            expect.assertions(1);
            
            Parser.mock.instances[0].parseString.mock.calls[0][1](error, null);
            return parsedPromise.then((result) => { fail(); }).catch((err)=>{ 
                expect(err).toBe(error);
            })
        })
    })

    describe("getSearchAction", () => {
        it("makes a request to bgg API and returns a promise", () => {
            const query = "Some <b>unsafe</b> query";

            const result = getSearchAction(query);
            expect(rp).toHaveBeenCalledTimes(1);
            
            const argumentToRp = rp.mock.calls[0][0];
            expect(argumentToRp).toHaveProperty("uri");
            expect(argumentToRp.uri).toMatch(
                new RegExp(
                    "^" + "https://boardgamegeek[.]com/xmlapi2/" + "search[?]" + 
                    "query=" + encodeURIComponent(query) + 
                    "&type=boardgame,boardgameexpansion" + "$"
                )
            );

            // It is necessarry to change implementation of Parser mock
            // so that parseString imediatelly calls its callback and
            // promise could be completed and mocks asserted.
            Parser.mockImplementationOnce((value) => {
                return {
                    parseString: (xml, cb) => {
                        cb(null, { item: "item" })
                    }
                }
            })
            
            expect.assertions(5);
            return result.then(()=>{
                expect(Parser).toBeCalledWith({
                    explicitArray: false,
                    explicitRoot: false,
                    mergeAttrs: true
                });
                expect(Parser.mock.instances.length).toBe(1);
            })
        })
    })
    describe("getSearchAction", () => {
        it("makes a request to bgg API and returns a promise", () => {
            const query = "Some <b>unsafe</b> query";

            const result = getSearchAction(query);
            expect(rp).toHaveBeenCalledTimes(1);
            
            const argumentToRp = rp.mock.calls[0][0];
            expect(argumentToRp).toHaveProperty("uri");
            expect(argumentToRp.uri).toMatch(
                new RegExp(
                    "^" + "https://boardgamegeek[.]com/xmlapi2/" + "search[?]" + 
                    "query=" + encodeURIComponent(query) + 
                    "&type=boardgame,boardgameexpansion" + "$"
                )
            );

            // It is necessarry to change implementation of Parser mock
            // so that parseString imediatelly calls its callback and
            // promise could be completed and mocks asserted.
            Parser.mockImplementationOnce((value) => {
                return {
                    parseString: (xml, cb) => {
                        cb(null, { item: "item" })
                    }
                }
            })
            
            expect.assertions(5);
            return result.then(()=>{
                expect(Parser).toBeCalledWith({
                    explicitArray: false,
                    explicitRoot: false,
                    mergeAttrs: true
                });
                expect(Parser.mock.instances.length).toBe(1);
            })
        })
    })
    
    describe("getThingAction", () => {
        it("makes a request to bgg API and returns a promise when called with single id", () => {
            const ids = 100;

            const result = getThingAction(ids);
            expect(rp).toHaveBeenCalledTimes(1);
            
            const argumentToRp = rp.mock.calls[0][0];
            expect(argumentToRp).toHaveProperty("uri");
            expect(argumentToRp.uri).toMatch(
                new RegExp(
                    "^" + "https://boardgamegeek[.]com/xmlapi2/" + "thing[?]" + 
                    "id=" + ids + 
                    "&stats=1" + "$"
                )
            );

            // It is necessarry to change implementation of Parser mock
            // so that parseString imediatelly calls its callback and
            // promise could be completed and mocks asserted.
            Parser.mockImplementationOnce((value) => {
                return {
                    parseString: (xml, cb) => {
                        cb(null, { item: "item" })
                    }
                }
            })
            
            expect.assertions(5);
            return result.then(()=>{
                expect(Parser).toBeCalledWith({
                    explicitArray: false,
                    explicitRoot: false,
                    mergeAttrs: true
                });
                expect(Parser.mock.instances.length).toBe(1);
            })
        })
        it("makes a request to bgg API and returns a promise when called with multiple ids", () => {
            const ids = [100, 200, 300, 400];

            const result = getThingAction(ids);
            expect(rp).toHaveBeenCalledTimes(1);
            
            const argumentToRp = rp.mock.calls[0][0];
            expect(argumentToRp).toHaveProperty("uri");
            expect(argumentToRp.uri).toMatch(
                new RegExp(
                    "^" + "https://boardgamegeek[.]com/xmlapi2/" + "thing[?]" + 
                    "id=" + ids + 
                    "&stats=1" + "$"
                )
            );
        })
        it("fails when called with mopre than 500 ids", () => {
            const ids = Array.from({length: 501});

            expect(() => {
                getThingAction(ids)
            }).toThrow();
            
        })
    })
});
