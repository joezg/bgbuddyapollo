import rp from 'request-promise';
import { Parser } from 'xml2js';

const BGG_API2_URI = "https://boardgamegeek.com/xmlapi2/";

export const parseBggXml = (xml) => {
    const xml2jsPromise = new Promise((resolve, reject)=>{
        const parser = new Parser({
            mergeAttrs: true,
            explicitArray: false,
            explicitRoot: false
        })
        parser.parseString(xml, function (err, result) {
            if (err){
                reject(err);
            } else {
                resolve(result.item);
            }
        });
    });
        
    return xml2jsPromise;
}

const getBggApi2Action = ({action, params}) => {
    let uri = `${BGG_API2_URI}${action}?`;
    const paramarray = [];
    for (let param in params){
        if (params.hasOwnProperty(param)){
            paramarray.push(`${param}=${params[param]}`);
        }
    }
    uri += paramarray.join('&');

    return rp({ uri }).then(parseBggXml);
}

export const getSearchAction = (query) => {
    return getBggApi2Action({
        action: "search",
        params: {
            query: encodeURIComponent(query),
            type: "boardgame,boardgameexpansion"
        }
    });
}

export const getThingAction = (ids) => {
    if (!ids.join){
        ids = [ids];
    } 

    if (ids.length > 500){
        throw new Error("Too many search results. Refine search query!");
    }

    return getBggApi2Action({
        action: "thing",
        params: {
            id: ids.join(','),
            stats: 1
        }
    })
}