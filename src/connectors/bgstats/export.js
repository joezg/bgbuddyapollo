

import getUserBuddyId from './getUserBuddyId';
import getPlayers from './getPlayers';
import { setFirstDate } from './constants';
import getLocations from './getLocations';
import getGames from './getGames';
import getMatches from './getMatches';

export default (db) => {

    const data =  {
        challenges: [ ],
        userInfo: {
            meRefId: null
        }
    }

    return setFirstDate(db).then(() => {
        return getUserBuddyId(db).then((user) => {
            data.userInfo.meRefId = user.buddy_id;
        });
    }).then(() => {
        return getPlayers(db).then((players) => {
            data.players = players;
        })
    }).then(() => {
        return getLocations(db).then((locations) => {
            data.locations = locations;
        })
    }).then(() => {
        return getGames(db).then((games) => {
            data.games = games;
        })
    }).then(() => {
        return getMatches(db).then((plays) => {
            data.plays = plays;
        })
    }).then(()=>{
        return data;
    })

}