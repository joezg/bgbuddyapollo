import formatDate from "./formatDate";
import constants from './constants';

export default (db) => {
    return db.user_location.find().then((locations) => {
        return locations.map((location) => {
            return {
                id: location.location_id,
                modificationDate: formatDate(constants().firstDate),
                name: location.name,
            }
        });
    });
}