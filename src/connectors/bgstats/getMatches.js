import formatDate from "./formatDate";
import constants from './constants';

export default (db) => {
    return db.userMatchScripts.getAllWithPlayers().then((matches) => {
        const plays = matches.reduce((acc, match) => {
            if (!acc[match.match_id]){
                acc[match.match_id] = {
                    ignored: !match.is_scored,
                    rating: 0,
                    scoringSetting: 0,
                    playerScores: [],
                    modificationDate: formatDate(match.date),
                    playDate: formatDate(match.date),
                    manualWinner: false,
                    locationRefId: match.location_id,
                    rounds: 0,
                    usesTeams: false,
                    bggId: 0,
                    entryDate: formatDate(match.date),
                    durationMin: 0,
                    nemestatsId: 0,
                    gameRefId: match.game_id,
                }
            }

            const player = {
                winner: match.winner === null ? false : match.winner,
                seatOrder: 0,
                score: match.result === null ? 0 : match.result,
                startPlayer: false,
                playerRefId: match.is_anonymous ? constants().anonymousId : match.buddy_id,
                rank: match.rank === null ? 0 : match.rank,
                newPlayer: false
            };

            acc[match.match_id].playerScores.push(player);
            
            return acc;
        }, {});

        return Object.values(plays)
    });
}