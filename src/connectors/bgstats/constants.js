let firstDate = new Date();
let anonymousId = 0;

export const setFirstDate = (db) => {
    return db.run("select min(date) from match").then((date) => {
        firstDate = date[0].min;
    })
}

export const setAnonymousId = (id) => {
    anonymousId = id;
}

export default () => {
    return {
        firstDate,
        anonymousId
    };
}