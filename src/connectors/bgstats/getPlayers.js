import formatDate from "./formatDate";
import constants, { setAnonymousId } from './constants';

export default (db) => {
    return db.user_buddy.find().then((buddies) => {
        let maxId = 0;
        const players = buddies.map((buddy) => {
            maxId = buddy.buddy_id > maxId ? buddy.buddy_id : maxId;
            return {
                id: buddy.buddy_id,
                isAnonymous: false,
                modificationDate: formatDate(constants().firstDate),
                name: buddy.name,
                bggUsername: ""
            }
        });

        const anonymousId = maxId + 1;
        players.push({
            id: anonymousId,
            isAnonymous: true,
            modificationDate: formatDate(constants().firstDate),
            name: "Anonymous player"
        });

        setAnonymousId(anonymousId);

        return players;
    });
}