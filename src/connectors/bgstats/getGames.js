import formatDate from "./formatDate";
import constants from './constants';

export default (db) => {
    return db.user_game.find().then((games) => {
        return games.map((game) => {
            return {
                id: game.game_id,
                modificationDate: formatDate(constants().firstDate),
                name: game.name,
                cooperative: false,
                highestWins: true,
                noPoints: false,
                usesTeams: false,
                bggYear: 0,
                bggId: 0
            }
        });
    });
}